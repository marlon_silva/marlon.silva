/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.BancoGradleDigital.entity;

import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author marlon.silva
 */
@Entity
@Table(name = "CARTAO")
public class Cartao {
    
     @Id
    @SequenceGenerator(allocationSize = 1, name = "CARTAO_TAB_SEQ",
            sequenceName = "CARTAO_TAB_SEQ")
    @GeneratedValue( generator = "CARTAO_TAB_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String chip;
    private Integer id_cliente;
    private Integer id_bandeira;
    private Integer id_emissor;
    private Integer vencimento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Integer getId_bandeira() {
        return id_bandeira;
    }

    public void setId_bandeira(Integer id_bandeira) {
        this.id_bandeira = id_bandeira;
    }

    public Integer getId_emissor() {
        return id_emissor;
    }

    public void setId_emissor(Integer id_emissor) {
        this.id_emissor = id_emissor;
    }

    public Integer getVencimento() {
        return vencimento;
    }

    public void setVencimento(Integer vencimento) {
        this.vencimento = vencimento;
    }
   
    
}
