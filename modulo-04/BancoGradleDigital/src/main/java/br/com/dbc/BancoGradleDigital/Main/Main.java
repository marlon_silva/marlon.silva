package br.com.dbc.BancoGradleDigital;

import br.com.dbc.BancoGradleDigital.entity.Banco;
import br.com.dbc.BancoGradleDigital.entity.Bandeira;
import br.com.dbc.BancoGradleDigital.entity.Cartao;
import br.com.dbc.BancoGradleDigital.entity.HibernateUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Main {
    /**
     * @param args the command line arguments
     */
     public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try{
        session = HibernateUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
         
        Banco banco = new Banco();
        banco.setNome("Bradesco");
        banco.setCodigo(003);
                
        session.save(banco);
        
        Bandeira bandeira = new Bandeira();
        bandeira.setNome("Elo");
        bandeira.setTaxa(10);
        
        session.save(banco);
       
        Cartao cartao = new Cartao();
        cartao.setChip("123");
        cartao.setVencimento(2023);
        session.save(cartao);

        transaction.commit();
        }catch (Exception ex) {
            if(transaction != null)
                    transaction.rollback();
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }finally{
            if(transaction != null)
            session.close();
            System.exit(0);
        }
    }
    private static final Logger LOG = Logger.getLogger(Main.class.getName());
}
