package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.Contato;
import br.com.dbc.lotr.entity.ElfoPerClass;
import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.HobbitPerClass;
import br.com.dbc.lotr.entity.HobbitTabelao;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.TipoContato;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.EnderecoDTO;
import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.dto.UsuarioPersonagemDTO;
import br.com.dbc.lotr.service.UsuarioService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import static org.hibernate.criterion.Projections.count;
import org.hibernate.criterion.Restrictions;

public class Main {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UsuarioService usuarioService = new UsuarioService();
        UsuarioPersonagemDTO  dto = new UsuarioPersonagemDTO();
        dto.setNomeUsuario("Gustavo");
        dto.setApelidoUsuario("Gustavo");
        dto.setCpfUsuario(Long.valueOf("123"));
        dto.setSenhaUsuario("123");
        
        EnderecoDTO enderecoDTO = new EnderecoDTO();
        enderecoDTO.setLogradouro("Rua do Gustavo");
        enderecoDTO.setNumero(123);
        enderecoDTO.setBairro("Bairro do Gustavo");
        enderecoDTO.setCidade("Cidade do gustavo");
        enderecoDTO.setComplemento("CEP 123");
        dto.setEndereco(enderecoDTO);
        
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setNome("Elfo do Gustavo");
        personagemDTO.setDanoHobbit(123d);
        dto.setPersonagem(personagemDTO);
        usuarioService.cadastrarUsuarioEPersonagem(dto);
        System.exit(0);
    
    }
    public static void oldMain(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try{
        session = HibernateUtil.getSession();
        transaction = session.beginTransaction();
         
        Usuario usuario = new Usuario();
        usuario.setNome("Antonio");
        usuario.setApelido("Tony");
        usuario.setCpf(123l);
        usuario.setSenha("456");
        
        Endereco endereco = new Endereco();
        endereco.setLogradouro("Rua Pablo Neruda");
        endereco.setNumero(370);
        endereco.setComplemento("Casa");
        endereco.setBairro("do antonio");
        endereco.setCidade("do antonio");
        
        Endereco endereco2 = new Endereco();
        endereco2.setLogradouro("Rua Pablo Neruda");
        endereco2.setNumero(100);
        endereco2.setComplemento("Casa");
        endereco2.setBairro("do antonio");
        endereco2.setCidade("do antonio");
        
        TipoContato tipoContato = new TipoContato();
        tipoContato.setNome("e-mail");
        tipoContato.setQuantidade(5);
        
        Contato contato = new Contato();
        contato.setTipoContato(tipoContato);
        contato.setUsuario(usuario);
        contato.setValor("antonio@email.com");
        
        usuario.pushContato(contato);
        usuario.pushEnderecos(endereco);
        
        session.save(usuario);
        
         ElfoTabelao elfoTabelao = new ElfoTabelao();
         elfoTabelao.setDanoElfo(100d);
         elfoTabelao.setNome("Legolas");
         
         session.save(elfoTabelao);
         
         ElfoPerClass elfoPerClass = new ElfoPerClass();
         elfoPerClass.setDanoElfo(100d);
         elfoPerClass.setNome("Melogas");
         
         session.save(elfoPerClass);
         
         ElfoJoin elfoJoin = new ElfoJoin();
         elfoJoin.setDanoElfo(100d);
         elfoJoin.setNome("Negolas");
         
         session.save(elfoJoin);
         
         HobbitJoin hobbitJoin = new HobbitJoin();
         hobbitJoin.setDanoHobbit(10d);
         hobbitJoin.setNome("Smigle");
         
         session.save(hobbitJoin);
         
         HobbitPerClass hobbitPerClass = new HobbitPerClass();
         hobbitPerClass.setDanoHobbit(10d);
         hobbitPerClass.setNome("Grodo");
         session.save(hobbitPerClass);
         
         HobbitTabelao hobbitTabelao = new HobbitTabelao();
         hobbitTabelao.setDanoHobbit(10d);
         hobbitTabelao.setNome("Frodo Bonceiro");
         
         session.save(hobbitTabelao);
         
         Criteria criteria = session.createCriteria(Usuario.class);
         criteria.createAlias("enderecos", "endereco");
         criteria.add(Restrictions.and(
                 Restrictions.ilike("endereco.bairro", "%do antonio"),
                 Restrictions.ilike("endereco.cidade", "%do antonio"))
         );
         
         
         
         List<Usuario> usuarios = criteria.list();
         
         System.out.println(usuario);
         usuarios.forEach(System.out::println);
         //conta quantidade de linhas
         criteria = session.createCriteria(Usuario.class);
         criteria.createAlias("enderecos", "endereco");
         criteria.add(Restrictions.and(
                 Restrictions.ilike("endereco.bairro", "%do antonio"),
                 Restrictions.ilike("endereco.cidade", "%do antonio"))
         );
         //conta quantidade de enderecos
         criteria.setProjection(Projections.rowCount());
            System.out.println(criteria.uniqueResult());
         System.out.println(String.format("Foram encontrados %s registros" + 
                 " com os critério especificados",
                 criteria.uniqueResult()));
         // OLHAR NO STAFF PARA COMPLETAR
         criteria.setProjection(Projections.sum("endereco.numero"));
            //System.out.println(String.format();
         
         usuarios =  session.createQuery("select u from Usuario u "
                 + " join.enderecos enderecos "
                 + " wher lower (endereco.cidade) like '%do antonio' "
                 + " and lower (endereco.bairro like '%do antonio' ")
                 .list();
            
         Long count = (Long)session.createQuery("selec u from Usuario u "
                 + " join u.enderecos endereco "
                 + "where lower (endereco.cidade) like '%do antonio' "
                 + "and lower (endereco.bairro) like '%do antonio' ").uniqueResult();
         System.out.println(String.format("Contamos com HQL %s enderecos",count));
         
         count = (Long)session.createQuery("selec count(endereco.id) from Usuario u "
                 + " join u.enderecos endereco "
                 + "where lower (endereco.cidade) like '%do antonio' "
                 + "and lower (endereco.bairro) like '%do antonio' ").uniqueResult();
            System.out.println(String.format("Contamos com HQL %s enderecos",count));
         
         transaction.commit();
        }catch (Exception ex) {
            if(transaction != null)
                    transaction.rollback();
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(1);
        }finally{
            if(transaction != null)
            session.close();
            
        }
        System.exit(0);
    }
    private static final Logger LOG = Logger.getLogger(Main.class.getName());
}
