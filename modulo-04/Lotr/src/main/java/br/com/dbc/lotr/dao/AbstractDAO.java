/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.AbstractEntity;
import br.com.dbc.lotr.entity.HibernateUtil;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author marlon.silva
 */
public abstract class  AbstractDAO <E extends AbstractEntity >{
    protected abstract Class<E> getEntityClass();
    
     public E criar(E entity){
        Session session = HibernateUtil.getSession();
       entity = (E) session.merge(entity);
       return entity;

      }
    
     public void atualizar(E entity){
        criar(entity);
    }
    
    public void remover(Integer id){
        Session session = HibernateUtil.getSession();
        session.createQuery("delete from " + getEntityClass().getSimpleName() 
         + " where id = " + id).executeUpdate();
    }
    
    public void remover(E entity){
        remover(entity.getId());
    }
    
    public E buscar(Integer id){
        Session session = HibernateUtil.getSession();
        return (E) session.createQuery("select e from " 
                 + getEntityClass().getSimpleName() + " e "
                 + " where id = " + id).uniqueResult();
    }
    
    public List<E> listar(){
        Session session = HibernateUtil.getSession();
        return session.createCriteria(getEntityClass()).list();
    }
}
