/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dto;

import br.com.dbc.lotr.entity.Usuario;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author marlon.silva
 */
public class EnderecoDTO {
@Id
    @SequenceGenerator(allocationSize = 1, name = "ENDERECO_SEQ",
            sequenceName = "ENDERECO_SEQ")
    @GeneratedValue( generator = "ENDERECO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String logradouro;
    private Integer numero;
    private String complemento;
    private String bairro;
    private String cidade;
    
    @ManyToMany(mappedBy = "enderecos")
    private List<Usuario> usuarios = new ArrayList();

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void pushUsuarios(Usuario...usuarios) {
        this.usuarios.addAll(Arrays.asList(usuarios));
        
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    
    
}
