/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author marlon.silva
 */
@Embeddable
public class LojaCredenciadorId {
    @Column(name= "ID_LOJA")
    private Integer idLoja;
    
    @Column(name = "ID_CREDENCIADOR")
    private Integer idCredenciador;

    public LojaCredenciadorId() {
    }

    public LojaCredenciadorId(Integer idLoja, Integer idLojaCredenciador) {
        this.idLoja = idLoja;
        this.idCredenciador = idCredenciador;
    }
    
   
}
