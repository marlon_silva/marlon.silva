/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author marlon.silva
 */
@Entity
@Table(name = "TIPOCONTATO")
public class TipoContato {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "TIPOCONTATO_SEQ",
            sequenceName = "TIPOCONTATO_SEQ")
    @GeneratedValue( generator = "TIPOCONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    
    private Integer id;
    private String nome;
    private Integer quantidade;

    @OneToOne( mappedBy = "tipoContato")
    private Contato contato;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
    
    
}
