/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author marlon.silva
 */
public class jdbc {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'USUARIO'").executeQuery();
            if (!rs.next()){   
           conn.prepareStatement("CREATE TABLE USUARIO(\n"
                    + " ID NUMBER NOT NULL PRIMARY KEY, \n"
                    + " NOME VARCHAR(100) NOT NULL, \n"
                    +" APELIDO VARCHAR(15) NOT NULL, \n"
                    +" SENHA VARCHAR(15) NOT NULL,\n"
                    +" CPF NUMBER(11) NOT NULL\n"
                    +")").execute();
            }
            PreparedStatement pst = conn.prepareStatement("inser into usuario(id, nome, apelido, senha, cpf) "
            + "values (usuario_seq.nextval, ?, ?, ?, ?)");
            pst.setString(1,"João");
            pst.setString(2,"Joãozinho");
            pst.setString(3, "123");
            pst.executeUpdate();
            
            rs = conn.prepareStatement("select * from usuario").executeQuery();
            while(rs.next()){
                System.out.println(String.format("Id do usuario: %$ ",rs.getInt("id")));
                System.out.println(String.format("Nome do usuario: %$ ",rs.getInt("nome")));
                System.out.println(String.format("Apelido do usuario: %$ ",rs.getInt("apelido")));
                System.out.println(String.format("senha do usuario: %$ ",rs.getInt("senha")));
                System.out.println(String.format("CPF do usuario: %$ ",rs.getInt("cpf")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(jdbc.class.getName())
                    .log(Level.SEVERE, "Erro na consulta do main", ex);
        }
           
    }
    
}
