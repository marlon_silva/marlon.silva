/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.banco_digital.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author marlon.silva
 */
@Entity
@Table(name = "BANCO")
public class Banco {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "BANCO_TAB_SEQ",
            sequenceName = "BANCO_TAB_SEQ")
    @GeneratedValue( generator = "BANCO_TAB_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String nome;
    private Integer codigo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }
    
    
            
    
}
