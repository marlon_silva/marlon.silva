package br.com.dbc.banco_digital;

import br.com.dbc.banco_digital.entity.Banco;
import br.com.dbc.banco_digital.entity.Bandeira;
import br.com.dbc.banco_digital.entity.HibernateUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Main {
    /**
     * @param args the command line arguments
     */
     public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try{
        session = HibernateUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
         
        Banco banco = new Banco();
        banco.setNome("Bradesco");
        banco.setCodigo(003);
                
        session.save(banco);
        
        Bandeira bandeira = new Bandeira();
        bandeira.setNome("Elo");
        bandeira.setTaxa(10);
        
        session.save(banco);

        transaction.commit();
        }catch (Exception ex) {
            if(transaction != null)
                    transaction.rollback();
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }finally{
            if(transaction != null)
            session.close();
            System.exit(0);
        }
    }
    private static final Logger LOG = Logger.getLogger(Main.class.getName());
}
