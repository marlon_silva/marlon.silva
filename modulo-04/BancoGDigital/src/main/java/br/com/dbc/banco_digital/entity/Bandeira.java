/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.banco_digital.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author marlon.silva
 */
@Entity
@Table(name = "BANDEIRA")
public class Bandeira {
     @Id
    @SequenceGenerator(allocationSize = 1, name = "BANDEIRA_TAB_SEQ",
            sequenceName = "BANDEIRA_TAB_SEQ")
    @GeneratedValue( generator = "BANDEIRA_TAB_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String nome;
    private Integer taxa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getTaxa() {
        return taxa;
    }

    public void setTaxa(Integer taxa) {
        this.taxa = taxa;
    }
    
}
