
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marlon.silva
 */
@Entity
@SequenceGenerator(allocationSize = 1, name = "angencia2_seq", sequenceName = "angencia2_seq")
public class Agencia2 {
    @Id
    @GeneratedValue(generator = "angencia2_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToOne
    private Banco banco;
    
}
