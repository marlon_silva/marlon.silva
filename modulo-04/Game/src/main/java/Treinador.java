
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marlon.silva
 */
@Entity
public class Treinador {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "treinador_seq", sequenceName = "treinador_seq")
    @GeneratedValue(generator = "treinador_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
      @ManyToMany
      @JoinTable(name = "TREINADOR_POKEMON", joinColumns = {
      @JoinColumn(name = "ID_TREINADOR")
              
      }, inverseJoinColumns = { 
      @JoinColumn (name = "ID_POKEMON")})
      private List<Pokemon> pokemons;
}
