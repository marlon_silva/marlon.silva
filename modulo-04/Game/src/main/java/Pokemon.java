
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import org.hibernate.annotations.ManyToAny;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marlon.silva
 */
@Entity
public class Pokemon {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "pokemon_seq", sequenceName = "pokemon_seq")
    @GeneratedValue(generator = "pokemon_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToMany (mappedBy = "pokemons")
    private List<Treinador> treinadores;
}
