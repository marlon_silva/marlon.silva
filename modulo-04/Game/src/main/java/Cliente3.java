
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marlon.silva
 */@Entity
public class Cliente3 {
    @SequenceGenerator(allocationSize = 1, name = "cliente3_seq", sequenceName = "cliente3_seq")
    @Id
    @GeneratedValue(generator = "cliente3_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
     @OneToOne(cascade = CascadeType.ALL)
     @JoinColumn(name = "id_crediario")
    private Crediario crediario;
}
