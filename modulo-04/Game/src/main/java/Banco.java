
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marlon.silva
 */
@Entity
@SequenceGenerator(allocationSize = 1, name = "banco_seq", sequenceName = "banco_seq")
public class Banco {
    @Id
    @GeneratedValue(generator = "banco_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
 
     @OneToMany(mappedBy = "banco", cascade = CascadeType.ALL)
    private List<Agencia2> agencia2 = new ArrayList<>();
}
