
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marlon.silva
 */
@Entity
public class sicredi {
     @SequenceGenerator(allocationSize = 1, name = "sicredi_seq", sequenceName = "sicredi_seq")
     @Id
    @GeneratedValue(generator = "sicredi_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private Integer distribuicao_sobras;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDistribuicao_sobras() {
        return distribuicao_sobras;
    }

    public void setDistribuicao_sobras(Integer distribuicao_sobras) {
        this.distribuicao_sobras = distribuicao_sobras;
    }
    
}
