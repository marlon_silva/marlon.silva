
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marlon.silva
 */
public class TreinadorPokemon {
   
    @Id
    @SequenceGenerator(allocationSize = 1, name = "treinador_pokemon_seq", sequenceName = "treinador_pokemon_seq")
    @GeneratedValue(generator = "treinador_pokemon_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
}
