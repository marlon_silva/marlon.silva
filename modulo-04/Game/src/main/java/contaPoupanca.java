
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marlon.silva
 */
@Entity
@Table(name = "CONTA_POUPANCA")
public class contaPoupanca extends conta{

    private Integer cnpj_pagador;

    public Integer getCnpj_pagador() {
        return cnpj_pagador;
    }

    public void setCnpj_pagador(Integer cnpj_pagador) {
        this.cnpj_pagador = cnpj_pagador;
    }
    
     
}
