<?xml version="1.0" encoding="utf-8" ?>
<!-- SQL XML created by WWW SQL Designer, https://github.com/ondras/wwwsqldesigner/ -->
<!-- Active URL: https://ondras.zarovi.cz/sql/demo/ -->
<sql>
<datatypes db="mysql">
	<group label="Numeric" color="rgb(238,238,170)">
		<type label="Integer" length="0" sql="INTEGER" quote=""/>
	 	<type label="TINYINT" length="0" sql="TINYINT" quote=""/>
	 	<type label="SMALLINT" length="0" sql="SMALLINT" quote=""/>
	 	<type label="MEDIUMINT" length="0" sql="MEDIUMINT" quote=""/>
	 	<type label="INT" length="0" sql="INT" quote=""/>
		<type label="BIGINT" length="0" sql="BIGINT" quote=""/>
		<type label="Decimal" length="1" sql="DECIMAL" re="DEC" quote=""/>
		<type label="Single precision" length="0" sql="FLOAT" quote=""/>
		<type label="Double precision" length="0" sql="DOUBLE" re="DOUBLE" quote=""/>
	</group>

	<group label="Character" color="rgb(255,200,200)">
		<type label="Char" length="1" sql="CHAR" quote="'"/>
		<type label="Varchar" length="1" sql="VARCHAR" quote="'"/>
		<type label="Text" length="0" sql="MEDIUMTEXT" re="TEXT" quote="'"/>
		<type label="Binary" length="1" sql="BINARY" quote="'"/>
		<type label="Varbinary" length="1" sql="VARBINARY" quote="'"/>
		<type label="BLOB" length="0" sql="BLOB" re="BLOB" quote="'"/>
	</group>

	<group label="Date &amp; Time" color="rgb(200,255,200)">
		<type label="Date" length="0" sql="DATE" quote="'"/>
		<type label="Time" length="0" sql="TIME" quote="'"/>
		<type label="Datetime" length="0" sql="DATETIME" quote="'"/>
		<type label="Year" length="0" sql="YEAR" quote=""/>
		<type label="Timestamp" length="0" sql="TIMESTAMP" quote="'"/>
	</group>
	
	<group label="Miscellaneous" color="rgb(200,200,255)">
		<type label="ENUM" length="1" sql="ENUM" quote=""/>
		<type label="SET" length="1" sql="SET" quote=""/>
		<type label="Bit" length="0" sql="bit" quote=""/>
	</group>
</datatypes><table x="42" y="247" name="cliente">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="nome" null="1" autoincrement="0">
<datatype>VARCHAR(100)</datatype>
<default>NULL</default></row>
<row name="cpf" null="0" autoincrement="0">
<datatype>TINYINT</datatype>
<default>NULL</default></row>
<row name="rg" null="0" autoincrement="0">
<datatype>TINYINT</datatype>
<default>NULL</default></row>
<row name="id_banco" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="banco" row="id" />
</row>
<row name="id_contatos" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="contatos" row="id" />
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="523" y="12" name="banco">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="nome" null="0" autoincrement="0">
<datatype>VARCHAR(100)</datatype>
</row>
<row name="codigo" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="815" y="328" name="conta">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="numero" null="0" autoincrement="0">
<datatype>VARCHAR(100)</datatype>
</row>
<row name="id_agencia" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="agencia" row="id" />
</row>
<row name="id_tipo_conta" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="tipo_conta" row="id" />
</row>
<row name="id_cliente" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="cliente" row="id" />
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="671" y="265" name="agencia">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="numero" null="0" autoincrement="0">
<datatype>TINYINT</datatype>
</row>
<row name="id_banco" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="banco" row="id" />
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="296" y="417" name="conta_conjunto">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="numero" null="0" autoincrement="0">
<datatype>TINYINT</datatype>
</row>
<row name="id_cliente1" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="cliente" row="id" />
</row>
<row name="id_cliente2" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="cliente" row="id" />
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="47" y="20" name="endereco">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="rua" null="0" autoincrement="0">
<datatype>VARCHAR(150)</datatype>
</row>
<row name="numero" null="0" autoincrement="0">
<datatype>SMALLINT</datatype>
</row>
<row name="bairro" null="0" autoincrement="0">
<datatype>VARCHAR(100)</datatype>
<default>'NULL'</default></row>
<row name="cidade" null="0" autoincrement="0">
<datatype>VARCHAR(100)</datatype>
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="972" y="136" name="transacoes_bancaricas">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="valor" null="1" autoincrement="0">
<datatype>BIGINT</datatype>
<default>NULL</default></row>
<row name="data" null="1" autoincrement="0">
<datatype>DATE</datatype>
<default>NULL</default></row>
<row name="id_cliente" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="cliente" row="id" />
</row>
<row name="id_conta_origem" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="conta" row="id" />
</row>
<row name="id_conta_destino" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="conta" row="id" />
</row>
<row name="id_tipo_transacao" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="tipo_transacao" row="id" />
</row>
<row name="id_emprestimo" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="emprestimo" row="id" />
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="801" y="17" name="usuario">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="nome" null="1" autoincrement="0">
<datatype>VARCHAR</datatype>
<default>NULL</default></row>
<row name="nome_usuario" null="1" autoincrement="0">
<datatype>VARCHAR</datatype>
<default>NULL</default></row>
<row name="senha" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="id_banco" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="banco" row="id" />
</row>
<row name="id_tipo_usuario" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="tipo_usuario" row="id" />
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="1003" y="12" name="tipo_usuario">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="gerente" null="0" autoincrement="0">
<datatype>VARCHAR(100)</datatype>
<default>'NULL'</default></row>
<row name="gerente_geral" null="0" autoincrement="0">
<datatype>VARCHAR(100)</datatype>
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="412" y="143" name="cliente_emprestimo_gerente">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="id_cliente" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="cliente" row="id" />
</row>
<row name="id_usuario" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="usuario" row="id" />
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="1023" y="375" name="tipo_transacao">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="saque" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="deposito" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="tranferencia" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="pagamento" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="308" y="265" name="cliente_agencia">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="id_agencia" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="agencia" row="id" />
</row>
<row name="id_cliente" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="cliente" row="id" />
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="228" y="31" name="endereco_cliente">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="id_endereco" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="endereco" row="id" />
</row>
<row name="id_cliente" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="cliente" row="id" />
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="511" y="386" name="tipo_conta">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="conta_fisica" null="1" autoincrement="0">
<datatype>VARCHAR</datatype>
<default>NULL</default></row>
<row name="conta_conjunta" null="1" autoincrement="0">
<datatype>VARCHAR</datatype>
<default>NULL</default></row>
<row name="conta_pj" null="1" autoincrement="0">
<datatype>VARCHAR</datatype>
<default>NULL</default></row>
<row name="id_conta_conjunto" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="conta_conjunto" row="id" />
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="777" y="506" name="emprestimo">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="qtd_parcelamento" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="id_tipo_status" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="tipo_status" row="id" />
</row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="996" y="552" name="tipo_status">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="aprovado" null="1" autoincrement="0">
<datatype>VARCHAR</datatype>
<default>NULL</default></row>
<row name="negado" null="1" autoincrement="0">
<datatype>VARCHAR</datatype>
<default>NULL</default></row>
<row name="aguardando_decisao" null="1" autoincrement="0">
<datatype>VARCHAR</datatype>
<default>NULL</default></row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
<table x="195" y="580" name="contatos">
<row name="id" null="1" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="telefone" null="1" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="Email" null="1" autoincrement="0">
<datatype>VARCHAR</datatype>
<default>NULL</default></row>
<key type="PRIMARY" name="">
<part>id</part>
</key>
</table>
</sql>
