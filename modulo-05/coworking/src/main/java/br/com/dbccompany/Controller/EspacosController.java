package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Espacos;
import br.com.dbccompany.Enum.tipoContratacao;
import br.com.dbccompany.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/api/espacos")
public class EspacosController {

    @Autowired
    private EspacosService espacosService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Espacos> lstEspacos(){
        return espacosService.allEspacos();
    }

    @PostMapping(value="/novo")
    @ResponseBody
    public Espacos novoEspaco( @RequestBody Espacos espacos) {
        return espacosService.salvar(espacos);
    }

    @GetMapping(value="/{nome}")
    @ResponseBody
    public Espacos buscaEspecificaNome(@PathVariable String nome) {
        return espacosService.buscarPorNome(nome);
    }

    @GetMapping(value="/{qtdPessoas}")
    @ResponseBody
    public Espacos buscaEspecificaQtdPessoas(@PathVariable long qtdPessoas) {
        return espacosService.buscarPorQtdPessoas(qtdPessoas);
    }

    @GetMapping(value="/{valor}")
    @ResponseBody
    public Espacos buscaEspecificaValor(@PathVariable double valor) {
        return espacosService.buscarPorValor(valor);
    }

}
