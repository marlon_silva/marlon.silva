package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Enum.tipoContratacao;
import br.com.dbccompany.Repository.ContratacaoRepository;
import br.com.dbccompany.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {
    @Autowired
    private ContratacaoRepository contratacaoRepository;
    @Autowired
    private EspacosRepository espacosRepository;

//
    @Transactional(rollbackFor = Exception.class)
    public double salvarContratacao(Contratacao contratacao) throws Exception {
    double valorEspaco = espacosRepository.findById(contratacao.getEspacos().getId()).get().getValor();
    long quantidade = contratacao.getQuantidade();
    long descontro = contratacao.getDesconto();
    double valorFinal = ( valorEspaco * quantidade) - descontro;
    contratacaoRepository.save(contratacao);
    return valorFinal;
}
    @Transactional(rollbackFor = Exception.class)
    public Optional<Contratacao> buscarContratacao(long id){
        return contratacaoRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Contratacao editarContratacao(long id, Contratacao contratacao) {
        return contratacaoRepository.save(contratacao);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Contratacao> allContratacao(){
        return (List<Contratacao>) contratacaoRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Contratacao buscarPorTipoContratacao(Enum<tipoContratacao> tipoContratacao){return contratacaoRepository.findByTipoContratacao(tipoContratacao);}

    @Transactional(rollbackFor = Exception.class)
    public Contratacao buscarPorQuantidade(long quantidade){return contratacaoRepository.findByQuantidade(quantidade);}

    @Transactional(rollbackFor = Exception.class)
    public Contratacao buscarPorDesconto(long desconto){return contratacaoRepository.findByDesconto(desconto);}

    @Transactional(rollbackFor = Exception.class)
    public Contratacao buscarPorPrazo(Date prazo){return contratacaoRepository.findByPrazo(prazo);}
}
