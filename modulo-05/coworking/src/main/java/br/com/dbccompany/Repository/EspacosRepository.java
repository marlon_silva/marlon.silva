package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Espacos;
import org.springframework.data.repository.CrudRepository;

public interface EspacosRepository extends CrudRepository<Espacos, Long> {

    Espacos findByNome (String nome);
    Espacos findByQtdPessoas (long qtdPessoas);
    Espacos findByValor (double valor);

}
