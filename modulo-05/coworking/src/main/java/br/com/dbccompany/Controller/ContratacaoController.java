package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Enum.tipoContratacao;
import br.com.dbccompany.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("api/contratacao")
public class ContratacaoController {

    @Autowired
    private ContratacaoService contratacaoService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contratacao> lstContratacao(){
        return contratacaoService.allContratacao();
    }

    @PostMapping(value="/novo")
    @ResponseBody
    public double novoContrato(Contratacao contratacao) throws Exception {
        return contratacaoService.salvarContratacao( contratacao );
    }

    @GetMapping(value="/{tipoContratacao}")
    @ResponseBody
    public Contratacao buscaEspecificaTipoContratacao(@PathVariable Enum<tipoContratacao> tipoContratacao) {
        return contratacaoService.buscarPorTipoContratacao(tipoContratacao);
    }

    @GetMapping(value="/{quantidade}")
    @ResponseBody
    public Contratacao buscaEspecificaQuantidade(@PathVariable long quantidade) {
        return contratacaoService.buscarPorQuantidade(quantidade);
    }

    @GetMapping(value="/{desconto}")
    @ResponseBody
    public Contratacao buscaEspecificaDesconto(@PathVariable long desconto) {
        return contratacaoService.buscarPorDesconto(desconto);
    }

    @GetMapping(value="/{prazo}")
    @ResponseBody
    public Contratacao buscaEspecificaPrazo(@PathVariable Date prazo) {
        return contratacaoService.buscarPorPrazo(prazo);
    }

}
