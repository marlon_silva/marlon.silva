package br.com.dbccompany.coworking.Integracao.AcessoTest;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Repository.AcessosRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;


import java.sql.Date;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BuscarAcessoIntegracaoTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AcessosRepository acessosRepository;

    @Test
    public void BuscarAcessoPorData(){
        Acessos acessos = new Acessos();
        Date date = new Date(2019-04-1);
        acessos.setData(date.toLocalDate());
        entityManager.persist(acessos);
        entityManager.flush();

        Acessos found = acessosRepository.findByData(acessos.getData());

        assertThat(found.getData()).isEqualTo(acessos.getData());
    }
}
