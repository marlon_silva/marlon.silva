package br.com.dbccompany.coworking.Integracao.UsuarioTest;

import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.UsuarioRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;


import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class NovoUsuarioIntegracaoTest {

    @MockBean

    private UsuarioRepository usuarioRepository;

    @Autowired
    ApplicationContext contex;

    @Before
    public void setUp() {
        Usuario usuario = new Usuario();
        usuario.setNome("Marlon");
        usuario.setEmail("marlon@coworking.com.br");
        usuario.setLogin("marlon.coworking");
        usuario.setSenha("123456");


        Mockito.when(usuarioRepository.findByNome(usuario.getNome())).thenReturn(usuario);
        Mockito.when(usuarioRepository.findByEmail(usuario.getEmail())).thenReturn(usuario);
        Mockito.when(usuarioRepository.findByLogin(usuario.getLogin())).thenReturn(usuario);
        Mockito.when(usuarioRepository.findBySenha(usuario.getSenha())).thenReturn(usuario);

    }

    @Test
    public void BuscaUsuarioIntegracaoPorNome() {
        String nome = "Marlon";
        Usuario found = usuarioRepository.findByNome(nome);

        assertThat(found.getNome()).isEqualTo(nome);
    }

    @Test
    public void BuscaUsuarioIntegracaoPorEmail(){
        String email = "marlon@coworking.com.br";
        Usuario found = usuarioRepository.findByEmail(email);

        assertThat(found.getEmail()).isEqualTo(email);

    }


}
