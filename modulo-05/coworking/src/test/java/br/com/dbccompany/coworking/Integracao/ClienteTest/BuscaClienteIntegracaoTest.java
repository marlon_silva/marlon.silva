package br.com.dbccompany.coworking.Integracao.ClienteTest;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.ClientesRepository;
import br.com.dbccompany.Repository.UsuarioRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;

import static org.assertj.core.api.Assertions.assertThat;


    @RunWith(SpringRunner.class)
    public class BuscaClienteIntegracaoTest {

        @MockBean

        private ClientesRepository clientesRepository;

        @Autowired
        ApplicationContext contex;

        @Before
        public void setUp() {
            Clientes clientes = new Clientes();

            clientes.setNome("Marlon");
            Date date = new Date(1990-11-13);
            clientes.setDataNascimento(date.toLocalDate());
            clientes.setCpf(123467891);


            Mockito.when(clientesRepository.findByNome(clientes.getNome())).thenReturn(clientes);
            Mockito.when(clientesRepository.findByDataNascimento(clientes.getDataNascimento())).thenReturn(clientes);
            Mockito.when(clientesRepository.findByCpf(clientes.getCpf())).thenReturn(clientes);


        }



        @Test
        public void BuscaClienteIntegracaoPorNome(){
            String nome = "Marlon";
            Clientes found = clientesRepository.findByNome(nome);

            assertThat(found.getNome()).isEqualTo(nome);

        }
        @Test
        public void BuscaClienteIntegracaoPorCpf(){
            long cpf = 123467891;
            Clientes found = clientesRepository.findByCpf(cpf);

            assertThat(found.getCpf()).isEqualTo(cpf);

        }


    }