package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Long>{
	
	Cliente findByCpf(long cpf);
	Cliente findByRg(long rg);

}
