package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.tipoTransacao;

public interface tipoTransacaoRepository extends CrudRepository<tipoTransacao, Long>{
	
	tipoTransacao findBySaque(long saque);
	tipoTransacao findByDeposito(long deposito);
	tipoTransacao findByTransferencia(long transferencia);
	tipoTransacao findByPagamento(long pagamento);

}
