package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Endereco;

public interface EnderecoRepository extends CrudRepository<Endereco, Long>{

	Endereco findByRua(String rua);
	Endereco findByNumero(long numero);
	Endereco findByBairro(String bairro);
	Endereco findByCidade(String cidade);

}
