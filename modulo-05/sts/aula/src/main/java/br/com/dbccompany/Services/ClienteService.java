package br.com.dbccompany.Services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Repository.ClienteRepository;

public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Cliente salvar(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
	private Optional<Cliente> buscarCliente(long id){
		return clienteRepository.findById(id);
	}
	
	private Cliente editarCliente(long id, Cliente cliente) {
		cliente.setId(id);
		return clienteRepository.save(cliente);
	}
}
