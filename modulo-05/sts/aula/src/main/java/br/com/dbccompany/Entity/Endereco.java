package br.com.dbccompany.Entity;


import java.util.Arrays;
import java.util.List;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;


@Entity
public class Endereco {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "Endereco_seq", sequenceName = "Endereco_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private String rua;
	private long numero;
	private String bairro;
	private String cidade;
	
	@ManyToMany
	@JoinTable
	(name = "cliente_endereco", 
	joinColumns = @JoinColumn(name = "endereco_id"),
	inverseJoinColumns = @JoinColumn(name = "cliente_id")
	)
	private List<Cliente> cliente;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public long getNumero() {
		return numero;
	}
	public void setNumero(long numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	
	
	public List<Cliente> getCliente() {
		return cliente;
	}
	public void pushCliente(Cliente... cliente) {
		this.cliente.addAll(Arrays.asList(cliente));
	}



	
	

}
