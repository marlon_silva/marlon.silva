package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;



// to do reafatorar unir com usuario
@Entity
public class Cliente {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "cliente_seq", sequenceName = "cliente_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private long cpf;
	private long rg;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getCpf() {
		return cpf;
	}
	public void setCpf(long cpf) {
		this.cpf = cpf;
	}
	public long getRg() {
		return rg;
	}
	public void setRg(long rg) {
		this.rg = rg;
	}
	
	
	
	public List<Contatos> getContatos() {
		return contatos;
	}
	public void pushContatos(Contatos... contatos) {
		this.contatos.addAll(Arrays.asList(contatos));
	}



	@ManyToOne
	@JoinColumn(name = "banco_id")
	private Banco banco;
	
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
	private List<Contatos> contatos = new ArrayList<>();
	
	@ManyToMany(mappedBy = "cliente")
	private List<Endereco> endereco = new ArrayList<Endereco>();
		
	@ManyToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
	private List<transacoesBancarias> trasacoesBancarias = new ArrayList<>();

	
	
}
