package br.com.dbccompany.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TIPO_TRANSACAO")
public class tipoTransacao {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "tipo_transacao_seq", sequenceName = "tipo_transacao_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	
	private long saque;
	private long deposito;
	private long transferencia;
	private long pagamento;
	public long getSaque() {
		return saque;
	}
	public void setSaque(long saque) {
		this.saque = saque;
	}
	public long getDeposito() {
		return deposito;
	}
	public void setDeposito(long deposito) {
		this.deposito = deposito;
	}
	public long getTransferencia() {
		return transferencia;
	}
	public void setTransferencia(long transferencia) {
		this.transferencia = transferencia;
	}
	public long getPagamento() {
		return pagamento;
	}
	public void setPagamento(long pagamento) {
		this.pagamento = pagamento;
	}
	
	@OneToOne
	@JoinColumn(name = "transacoesBancarias")
	private transacoesBancarias	transacoesBancarias;

}
