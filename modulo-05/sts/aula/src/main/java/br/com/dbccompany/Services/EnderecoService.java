package br.com.dbccompany.Services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Repository.EnderecoRepository;

public class EnderecoService {

	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Endereco salvar(Endereco endereco) {
		return enderecoRepository.save(endereco);
	}
	private Optional<Endereco> buscarEndereco(long id){
		return enderecoRepository.findById(id);
	}
	
	private Endereco editarEndereco(long id, Endereco endereco) {
		endereco.setId(id);
		return enderecoRepository.save(endereco);
	}
}
