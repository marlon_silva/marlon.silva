package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	
	Usuario findByNome(String nome);
	Usuario findByNomeUsuario(String nomeUsuario);
	Usuario findBySenha(long senha);

}
