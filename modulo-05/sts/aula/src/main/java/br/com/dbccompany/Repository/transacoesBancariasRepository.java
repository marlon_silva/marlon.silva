package br.com.dbccompany.Repository;

import java.sql.Date;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.transacoesBancarias;

public interface transacoesBancariasRepository extends CrudRepository<transacoesBancarias, Long>{

	transacoesBancarias findByValor(int valor);
	transacoesBancarias findByData (Date data);
}
