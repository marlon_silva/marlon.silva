package br.com.dbccompany.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Contatos {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "contatos_seq", sequenceName = "contatos_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	
	private long id;
	private int telefone;
	private String email;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getTelefone() {
		return telefone;
	}
	public void setTelefone(int telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@ManyToOne
	@JoinColumn(name = "cliente_id")
    private Cliente cliente;
			
	
}
