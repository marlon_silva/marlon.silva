package br.com.dbccompany.Services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


import br.com.dbccompany.Entity.Pais;
import br.com.dbccompany.Repository.PaisRepository;



public class PaisService {

	@Autowired
	private PaisRepository paisRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private Pais salvar(Pais pais) {
		return paisRepository.save(pais);
	}
	
	private Optional<Pais> buscarPais(long id){
		return paisRepository.findById(id);
	}
	private Pais editarPais(long id, Pais pais) {
		pais.setId(id);
		return paisRepository.save(pais);
	}
}

