package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;

public interface AgenciaRepository extends CrudRepository<Agencia, Long>{
	
	Agencia findByNumero(long numero);
}
