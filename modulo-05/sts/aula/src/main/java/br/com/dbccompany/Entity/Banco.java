package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Banco {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "banco_seq", sequenceName = "banco_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "CODIGO" )
	private long codigo;
	
	@Column(name = "NOME" )
	private String nome;

	@OneToMany(mappedBy = "banco", cascade = CascadeType.ALL)
	    private List<Agencia> agencia = new ArrayList<>();
	
	@OneToMany(mappedBy = "banco", cascade = CascadeType.ALL)
		private List<Cliente> cliente =new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Agencia> getAgencia() {
		return agencia;
	}

	public void setAgencia(List<Agencia> agencia) {
		this.agencia = agencia;
	}

	public List<Cliente> getCliente() {
		return cliente;
	}

	public void setCliente(List<Cliente> cliente) {
		this.cliente = cliente;
	}
	
	public void pushCliente(Cliente... cliente) {
		this.cliente.addAll(Arrays.asList(cliente));
	}
	
	
}
