package br.com.dbccompany.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Agencia;

import br.com.dbccompany.Repository.AgenciaRepository;

@Service
public class AgenciaService {

	@Autowired
	private AgenciaRepository agenciaRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Agencia salvar(Agencia agencia) {
		return agenciaRepository.save(agencia);
	}
	
	public Optional<Agencia> buscarAgencia(long id){
		return agenciaRepository.findById(id);
	}
	
	public Agencia editarAgencia(long id, Agencia agencia) {
		agencia.setId(id);
		return agenciaRepository.save(agencia);
	}
	
	public List<Agencia> allAgencias(){
		return (List<Agencia>) agenciaRepository.findAll();
	}
	public Agencia buscarPorNumero(long numero) {
		return agenciaRepository.findByNumero(numero);
	}
	
}
