package br.com.dbccompany.Services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Pais;
import br.com.dbccompany.Entity.tipoTransacao;
import br.com.dbccompany.Repository.tipoTransacaoRepository;

public class tipoTransacaoService {

	@Autowired
	private tipoTransacaoRepository tipoTransacaoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	private tipoTransacao salvar(tipoTransacao tipoTransacao) {
		return tipoTransacaoRepository.save(tipoTransacao);
	}
	
	private Optional<tipoTransacao> buscarTipoTransacao(long id){
		return tipoTransacaoRepository.findById(id);
	}
	private tipoTransacao editarTipoTransacao(long id, tipoTransacao tipoTransacao) {
		//tipoTransacao.set
		return tipoTransacaoRepository.save(tipoTransacao);
	}
}
