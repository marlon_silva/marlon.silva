package br.com.dbccompany.Entity;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "Transacoes_Bancarias")
public class transacoesBancarias {
	@SequenceGenerator(allocationSize = 1, name = "Transacoes_Bancarias_seq", sequenceName = "Transacoes_Bancarias_seq")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	
	private long id;
	private int valor;
	private Date data;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	@ManyToOne
	@JoinColumn(name = "cliente_id")
    private Cliente cliente;

	@OneToOne
	@JoinColumn(name = "tipoTransacao")
	private tipoTransacao	tipoTransacao;

}
