package br.com.dbccompany.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Usuario {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "usuario_seq", sequenceName = "usuario_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	
	private long id;
	private String nome;
	private String nomeUsuario;
	private long senha;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNomeUsuario() {
		return nomeUsuario;
	}
	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}
	public long getSenha() {
		return senha;
	}
	public void setSenha(long senha) {
		this.senha = senha;
	}
	
	

}
