package br.com.dbccompany.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Services.BancoService;



@Controller
@RequestMapping("/api/banco")
public class BancoController {

	@Autowired
	BancoService bancoService;
	
	@GetMapping(value="/")
	@ResponseBody
	public List<Banco> lstBancos(){
		return bancoService.allBancos();
	}
	
	@PostMapping(value="/novo")
	@ResponseBody
	public Banco novoBanco( @RequestBody Banco banco) {
		return bancoService.salvar(banco);
	}
	
	@GetMapping(value="/{codigo}")
	@ResponseBody
	public Banco bancoEspecifica(@PathVariable long codigo) {
		return bancoService.buscarPorCodigo(codigo);
	}
	
	@PutMapping(value="/editar/{id}")
	@ResponseBody
	public Banco editarBanco(@PathVariable long id, @RequestBody Banco banco) {
		return bancoService.editarBanco(id, banco);
		
	}
}
