package br.com.dbccompany.Entity;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;


@Entity
public class Agencia {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "agencia_seq", sequenceName = "agencia_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	private long numero;
	
	@ManyToOne
	@JoinColumn(name = "banco_id")
	private Banco banco;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getNumero() {
		return numero;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	
	
}
