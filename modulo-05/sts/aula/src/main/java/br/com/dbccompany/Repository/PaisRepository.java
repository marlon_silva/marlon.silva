package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Pais;

public interface PaisRepository extends CrudRepository<Pais, Long>{
	
	Pais findByNome(String nome);

}
