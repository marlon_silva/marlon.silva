package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Repository.SeguradosRepository;

@Service
public class SeguradosService extends PessoasService {
	
	@Autowired
	private SeguradosRepository seguradosRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Segurados salvar(Segurados segurados) {
		return seguradosRepository.save(segurados);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Optional<Segurados> bucarSegurados(long id){
		return seguradosRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Segurados editarSegurados(long id, Segurados segurados) {
		return seguradosRepository.save(segurados);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public List<Segurados> allSegurados(){
		return (List<Segurados>) seguradosRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Segurados buscarPorQtdServico(long qtdServico) {
		return seguradosRepository.findByQtdServicos(qtdServico);
	}
	@Transactional(rollbackFor =Exception.class)
	public Segurados buscarPorNome(String nome) {
		return (Segurados) seguradosRepository.findByNome(nome);
	}
	
}
