package br.com.dbccompany.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.Pessoas;
import br.com.dbccompany.Service.CorretorService;


@Controller
@RequestMapping("/api/corretor")
public class CorretorController {
	
	@Autowired
	private CorretorService corretorService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Corretor> lstCorretor(){
		return corretorService.allCorretor();
	}
	@PostMapping(value="/novo")
	@ResponseBody
	public Corretor novoCorretor( @RequestBody Corretor corretor) {
		return corretorService.salvar(corretor);
	}
	
	@GetMapping(value="/{cargo}")
	@ResponseBody
	public Corretor bancoEspecifica(@PathVariable String cargo) {
		return corretorService.buscarPorCargo(cargo);
	}
	
	@GetMapping(value="/{nome}")
	@ResponseBody
	public Corretor buscaEspecificaNome(@PathVariable String nome) {
		return corretorService.buscarPorNome(nome);
	}
	
	@GetMapping(value="/{cpf}")
	@ResponseBody
	public Corretor buscaEspecificaCpf(@PathVariable long cpf) {
		return corretorService.buscarPorCpf(cpf);
	}
	
	@GetMapping(value="/{email}")
	@ResponseBody
	public Pessoas buscaEspecificaEmail(@PathVariable String email) {
		return corretorService.buscarPorEmail(email);
	}
	@GetMapping(value="/{telefone}")
	@ResponseBody
	public Pessoas buscaEspecificaTelefone(@PathVariable long telefone) {
		return corretorService.buscarPorTelefone(telefone);
	}
}
