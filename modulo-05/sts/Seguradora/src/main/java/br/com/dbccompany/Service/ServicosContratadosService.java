package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Repository.ServicoContratadoRepository;

@Service
public class ServicosContratadosService {

	@Autowired
	private ServicoContratadoRepository servicoContratadoRepository;
//	
//	@Autowired
//	private CorretorRepository corretorRepository;
//	
//	@Autowired
//	private SeguradosRepository seguradosRepository;
	
	//@Transactional(rollbackFor = Exception.class)
	//public ServicoContratado salvar(ServicoContratado servico) {
		//Corretor corretor = corretorRepository.findById(servico.getPessoaCorretor().getId().get();
		//Segurados segurados = seguradosRepository.findById(servico.getPessoaCorretor().getId().get;
		//segurados.setQtdServicos(qtdServicos);
		//corretor.setComissao(corretor);
	//}
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado salvar(ServicoContratado servicoContratado) {
		return servicoContratadoRepository.save(servicoContratado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Optional<ServicoContratado> bucarServicoContratado(long id){
		return servicoContratadoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado editarServicoContratado(long id, ServicoContratado servicoContratado) {
		return servicoContratadoRepository.save(servicoContratado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public List<ServicoContratado> allSegurados(){
		return (List<ServicoContratado>) servicoContratadoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado buscarPorDescricao(String descricao) {
		return servicoContratadoRepository.findByDescricao(descricao);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado buscarPorValor(long valor) {
		return servicoContratadoRepository.findByValor(valor);
	}
	
}
