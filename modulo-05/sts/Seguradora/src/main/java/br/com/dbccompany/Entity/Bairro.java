package br.com.dbccompany.Entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Bairro {
	 @Id
	 @SequenceGenerator(allocationSize = 1, name="bairro_seq", sequenceName = "bairro_seq")
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 private long id;
	 
	 @Column( name = "NOME", nullable = false )
	 private String nome;
	 
	@ManyToOne
	@JoinColumn(name = "cidade_id")
	private Cidade cidade;
		 
	@OneToOne
	@JoinColumn(name = "endereco")
	private Endereco endereco;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	 
	
	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

}
