package br.com.dbccompany.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Service.SeguradosService;

@Controller
@RequestMapping("/api/segurados")
public class SeguradosController {

	@Autowired
	public SeguradosService seguradosService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Segurados> lstSegurados(){
		return seguradosService.allSegurados();
	}

	@PostMapping(value="/novo")
	@ResponseBody
	public Segurados novaSegurados( @RequestBody Segurados segurados) {
		return seguradosService.salvar(segurados);
	}
	
	@GetMapping(value="/{nome}")
	@ResponseBody
	public Segurados bancoEspecifica(@PathVariable String nome) {
		return (Segurados) seguradosService.buscarPorNome(nome);
	}
	// primeiro cast :)

	
}
