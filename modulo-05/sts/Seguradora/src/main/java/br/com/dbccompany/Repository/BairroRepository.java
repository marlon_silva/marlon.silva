package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Bairro;

public interface BairroRepository extends CrudRepository<Bairro, Long> {
	
	Bairro findByNome(String nome);
}
