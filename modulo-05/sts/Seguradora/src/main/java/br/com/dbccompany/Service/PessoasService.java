package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Pessoas;
import br.com.dbccompany.Repository.PessoasRepository;

@Service
public class PessoasService {

	@Autowired
	public PessoasRepository<Pessoas> pessoasRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas salvar(Pessoas pessoas) {
		return pessoasRepository.save(pessoas);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Optional<Pessoas> bucarPessoas(long id){
		return pessoasRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas editarPessoas(long id, Pessoas pessoas) {
		return pessoasRepository.save(pessoas);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public List<Pessoas> allPessoas(){
		return (List<Pessoas>) pessoasRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas buscarPorNome(String nome) {
		return pessoasRepository.findByNome(nome);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas buscarPorCpf(long  cpf) {
		return pessoasRepository.findByCpf(cpf);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas buscarPorPai(String pai) {
		return pessoasRepository.findByPai(pai);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas buscarPorMae(String mae) {
		return pessoasRepository.findByMae(mae);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas buscarPortelefone(long telefone) {
		return pessoasRepository.findByTelefone(telefone);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas buscarPorEmail(String email) {
		return pessoasRepository.findByemail(email);
	}
	
}
