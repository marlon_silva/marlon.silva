package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;


import br.com.dbccompany.Entity.Endereco;

public interface EnderecoRepository extends CrudRepository<Endereco, Long>{
	 
	Endereco findByLogradouro(String logradouro);
	Endereco findByNumero(long numero);
	Endereco findByComplemento(String complemento);
}
