package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Servicos;

public interface ServicosRepository extends CrudRepository<Servicos, Long> {
	
	Servicos findByNome(String nome);
	Servicos findByDescricao(String descricao);
	Servicos findByValorPadrao(long valorPadrao);
}
