package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Repository.CorretorRepository;

@Service
public class CorretorService {
	
	@Autowired
	private CorretorRepository corretorRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor salvar(Corretor corretor) {
		return corretorRepository.save(corretor);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Optional<Corretor> bucarCorretor(long id){
		return corretorRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor editarCorretor(long id, Corretor corretor) {
		return corretorRepository.save(corretor);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public List<Corretor> allCorretor(){
		return (List<Corretor>) corretorRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor buscarPorCargo(String cargo) {
		return corretorRepository.findByCargo(cargo);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor buscarPorComissao(String comissao) {
		return corretorRepository.findByCargo(comissao);
	}
	@Transactional(rollbackFor = Exception.class)
	public Corretor buscarPorNome(String nome) {
		 return corretorRepository.findByNome(nome);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor buscarPorCpf(long cpf) {
		return corretorRepository.findByCpf(cpf);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor buscarPorEmail(String email) {
		return corretorRepository.findByemail(email);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor buscarPorTelefone(long telefone) {
		return corretorRepository.findByTelefone(telefone);
	}
	
}
