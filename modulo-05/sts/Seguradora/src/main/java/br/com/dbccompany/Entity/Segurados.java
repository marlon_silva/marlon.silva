package br.com.dbccompany.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
@PrimaryKeyJoinColumn(name = "id_pessoas")
public class Segurados extends Pessoas {
	
	@GeneratedValue( strategy = GenerationType.SEQUENCE)
		
	private long qtdServicos;
	
	@ManyToOne
	@JoinColumn(name = "corretor_id")
	private Corretor corretor;
	

	public long getQtdServicos() {
		return qtdServicos;
	}

	public void setQtdServicos(long qtdServicos) {
		this.qtdServicos = qtdServicos;
	}

	public Corretor getCorretor() {
		return corretor;
	}

	public void setCorretor(Corretor corretor) {
		this.corretor = corretor;
	}

	
}
