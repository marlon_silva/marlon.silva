package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;


@Entity
@PrimaryKeyJoinColumn(name = "ID_PESSOAS")
public class Corretor extends Pessoas{
	
	
	@SequenceGenerator(allocationSize = 1, name = "corretor_seq", sequenceName = "corretor_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	
	@Column( name = "CARGO", nullable = false )
	private String cargo;
	
	@Column( name = "COMISSAO", nullable = false )
	private long comissao;
	
	@OneToMany(mappedBy = "corretor", cascade = CascadeType.ALL)
	private List<Segurados> segurados = new ArrayList<>();
	
	@OneToMany(mappedBy = "corretor", cascade = CascadeType.ALL)
	private List<ServicoContratado> servicoContratados = new ArrayList<>();
	
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public long getComissao() {
		return comissao;
	}
	public void setComissao(long comissao) {
		this.comissao = comissao;
	}
	
}
