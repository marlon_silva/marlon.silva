package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Servicos {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "servico_seq", sequenceName = "servico_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column( name = "NOME", nullable = false )
	private String nome;
	
	@Column( name = "DESCRICAO", nullable = false )
	private String descricao;
	
	@Column( name = "VALOR_PADRAO", nullable = false )
	private long valorPadrao;
	
	@ManyToMany(mappedBy = "servicos")
	private List<Seguradora> seguradora = new ArrayList<>();
	
	@OneToOne
	@JoinColumn(name = "servicoContratado")
	private ServicoContratado servicoContratado;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public long getValorPadrao() {
		return valorPadrao;
	}
	public void setValorPadrao(long valorPadrao) {
		this.valorPadrao = valorPadrao;
	}
	
	public List<Seguradora> getSeguradora() {
		return seguradora;
	}
	public void pushSeguradora(Seguradora...seguradora) {
		this.seguradora.addAll(Arrays.asList(seguradora));
	}
	
	public ServicoContratado getServicoContratado() {
		return servicoContratado;
	}
	public void setServicoContratado(ServicoContratado servicoContratado) {
		this.servicoContratado = servicoContratado;
	}
	
}
