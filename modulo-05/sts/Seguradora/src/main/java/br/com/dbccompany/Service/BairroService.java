package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Repository.BairroRepository;

@Service
public class BairroService {

	@Autowired
	private BairroRepository bairroRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Bairro salvar(Bairro bairro) {
		return bairroRepository.save(bairro);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Optional<Bairro> buscarBairro(long id){
		return bairroRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Bairro editarBairro(long id,Bairro bairro) {
		return bairroRepository.save(bairro);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public List<Bairro> allBairro(){
		return (List<Bairro>) bairroRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Bairro buscarPorNome(String nome) {
		return bairroRepository.findByNome(nome);
	}
}
