package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Seguradora;

public interface SeguradoraRepository extends CrudRepository<Seguradora, Long>{
	
	Seguradora findByNome(String nome);
	Seguradora findByCnpj(long cnpj);

}
