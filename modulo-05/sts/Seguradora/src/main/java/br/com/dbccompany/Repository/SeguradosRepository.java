
package br.com.dbccompany.Repository;




import br.com.dbccompany.Entity.Segurados;

public interface SeguradosRepository extends PessoasRepository<Segurados>{
	
	Segurados findByQtdServicos(long qtdServicos);
	
}