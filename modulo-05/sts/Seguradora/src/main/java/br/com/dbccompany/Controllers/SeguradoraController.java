package br.com.dbccompany.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Service.SeguradoraService;

@Controller
@RequestMapping("/api/seguradoras")
public class SeguradoraController {
	
	@Autowired
	private SeguradoraService seguradoraService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Seguradora> lstSeguradora(){
		return seguradoraService.allSeguradora();
	}
	@PostMapping(value="/nova")
	@ResponseBody
	public Seguradora novaSeguradora( @RequestBody Seguradora seguradora) {
		return seguradoraService.salvar(seguradora);
	}
	
	@GetMapping(value="/{cnpj}")
	@ResponseBody
	public Seguradora bancoEspecifica(@PathVariable Long cnpj) {
		return seguradoraService.buscarPorCnpj(cnpj);
	}
}
