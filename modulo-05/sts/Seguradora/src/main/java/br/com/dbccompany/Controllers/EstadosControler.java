package br.com.dbccompany.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Estado;

import br.com.dbccompany.Service.EstadoService;


@Controller
@RequestMapping("/api/estados")
public class EstadosControler {

	@Autowired
	public EstadoService estadoService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Estado> lstEstado(){
		return estadoService.allEstados();
	}
	@PostMapping(value="/nova")
	@ResponseBody
	public Estado novaEstado( @RequestBody Estado estado) {
		return estadoService.salvar(estado);
	}
	
	@GetMapping(value="/{nome}")
	@ResponseBody
	public Estado bancoEspecifica(@PathVariable String nome) {
		return estadoService.buscarPorNome(nome);
	}
	
	
}
