package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Pessoas;

public interface PessoasRepository <E extends Pessoas> extends CrudRepository<E, Long>{
	E findByNome(String nome);
	E findByCpf(long cpf);
	E findByPai(String pai);
	E findByMae(String mae);
	E findByTelefone(long telefone);
	E findByemail(String email);

}
