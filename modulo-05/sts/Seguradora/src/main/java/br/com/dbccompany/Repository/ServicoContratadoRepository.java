package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.ServicoContratado;

public interface ServicoContratadoRepository extends CrudRepository<ServicoContratado, Long>{

	ServicoContratado findByDescricao(String descricao);
	ServicoContratado findByValor(long valor);
	
}
