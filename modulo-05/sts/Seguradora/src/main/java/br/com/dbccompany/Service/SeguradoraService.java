package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Repository.SeguradoraRepository;

@Service
public class SeguradoraService {
	
	@Autowired
	private SeguradoraRepository seguradoraRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Seguradora salvar(Seguradora seguradora) {
		return seguradoraRepository.save(seguradora);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Optional<Seguradora> bucarSeguradora(long id){
		return seguradoraRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Seguradora editarSeguradora(long id, Seguradora seguradora) {
		return seguradoraRepository.save(seguradora);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public List<Seguradora> allSeguradora(){
		return (List<Seguradora>) seguradoraRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Seguradora buscarPorNome(String nome) {
		return seguradoraRepository.findByNome(nome);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Seguradora buscarPorCnpj(long  cnpj) {
		return seguradoraRepository.findByCnpj(cnpj);
	}
	
}
