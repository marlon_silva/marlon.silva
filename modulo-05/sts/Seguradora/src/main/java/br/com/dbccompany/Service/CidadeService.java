package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Repository.CidadeRepository;

@Service
public class CidadeService {
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Cidade salvar(Cidade cidade) {
		return cidadeRepository.save(cidade);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Optional<Cidade> buscarCidade(long id){
		return cidadeRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Cidade editarCidade(long id, Cidade cidade) {
		return cidadeRepository.save(cidade);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public List<Cidade> allCidade(){
		return (List<Cidade>) cidadeRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Cidade buscarPorNome(String nome) {
		return cidadeRepository.findByNome(nome);
	}
	

}
