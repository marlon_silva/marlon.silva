package br.com.dbccompany.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Pessoas;
import br.com.dbccompany.Service.PessoasService;

@Controller
@RequestMapping("/api/pessoas")
public class PessoasController {
	
	@Autowired
	public PessoasService pessoasService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Pessoas> lstPessoas(){
		return pessoasService.allPessoas();
	}
	@PostMapping(value="/nova")
	@ResponseBody
	public Pessoas novaPessoas( @RequestBody Pessoas pessoas) {
		return pessoasService.salvar(pessoas);
	}
	
	@GetMapping(value="/{nome}")
	@ResponseBody
	public Pessoas bancoEspecifica(@PathVariable String nome) {
		return pessoasService.buscarPorNome(nome);
	}
	
	@GetMapping(value="/{email}")
	@ResponseBody
	public Pessoas buscaEspecifica(@PathVariable String email) {
		return pessoasService.buscarPorEmail(email);
	}
	

}