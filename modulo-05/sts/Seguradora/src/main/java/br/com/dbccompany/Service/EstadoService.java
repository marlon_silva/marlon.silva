package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Repository.EstadoRepository;

@Service
public class EstadoService {
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Estado salvar(Estado estado) {
		return estadoRepository.save(estado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Optional<Estado> buscarEstado(long id){
		return estadoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Estado editarEstado(long id, Estado estado) {
		return estadoRepository.save(estado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public List<Estado> allEstados(){
		return (List<Estado>) estadoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Estado buscarPorNome(String nome) {
		return estadoRepository.findByNome(nome);
	}
}
