package br.com.dbccompany.Entity;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Endereco {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "endereco_seq", sequenceName = "endereco_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column( name = "LOGRADOURO", nullable = false )
	private String logradouro;
	
	@Column( name = "NUMERO", nullable = false )
	private long numero;
	
	@Column( name = "COMPLEMENTO", nullable = false )
	private String complemento;
	
	@ManyToMany
	@JoinTable
	(name = "endereco_pessoas", 
	joinColumns = @JoinColumn(name = "endereco_id"),
	inverseJoinColumns = @JoinColumn(name = "pessoas_id")
	)
	private List<Pessoas> pessoas = new ArrayList<Pessoas>();
	
	@OneToOne
	@JoinColumn(name = "seguradora")
	private Seguradora seguradora;
	
	@OneToOne
	@JoinColumn(name = "bairro")
	private Bairro bairro;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public long getNumero() {
		return numero;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public List<Pessoas> getPessoas() {
		return pessoas;
	}

	public void pushPessoas(Pessoas... pessoas) {
		this.pessoas.addAll(Arrays.asList(pessoas));;
	}

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}

}
