package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Pessoas {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "pessoas_seq", sequenceName = "pessoas_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	
	private long id;
	
	@Column( name = "NOME", nullable = false )
	private String nome;
	
	@Column( name = "CPF", nullable = false, unique = true )
	private long cpf;
	
	@Column( name = "PAI", nullable = false )
	private  String pai;
	
	@Column( name = "MAE", nullable = false )
	private String mae;
	
	@Column( name = "TELEFONE", nullable = false )
	private long telefone;
	
	@Column( name = "EMAIL", nullable = false, unique = true)
	private String email;
	
	@ManyToMany(mappedBy = "pessoas")
	private List<Endereco> endereco = new ArrayList<Endereco>();
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public long getTelefone() {
		return telefone;
	}

	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	} 

	public List<Endereco> getEndereco() {
		return endereco;
	}

	public void pushEndereco(Endereco...endereco) {
		this.endereco.addAll(Arrays.asList(endereco));
	}
	
}
