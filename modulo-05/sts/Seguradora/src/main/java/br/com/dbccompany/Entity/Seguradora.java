package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Seguradora {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "seguradora_seq", sequenceName = "seguradora_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	@Column( name = "NOME", nullable = false )
	private String nome;
	
	@Column( name = "CNPJ", nullable = false, unique = true )
	private long cnpj;
	
	@OneToOne
	@JoinColumn(name = "seguradora")
	private EnderecoSeguradora enderecoSeguradora;
	
	@ManyToMany
	@JoinTable(name ="segurados_servicos",
		joinColumns = {
				@JoinColumn(name ="seguradora_id")},
		
		inverseJoinColumns = {
				@JoinColumn(name = "servicos_id")})
	private List<Servicos> servicos = new ArrayList<>();
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public long getCnpj() {
		return cnpj;
	}
	public void setCnpj(long cnpj) {
		this.cnpj = cnpj;
	}
	
	public EnderecoSeguradora getEnderecoSeguradora() {
		return enderecoSeguradora;
	}
	public void setEnderecoSeguradora(EnderecoSeguradora enderecoSeguradora) {
		this.enderecoSeguradora = enderecoSeguradora;
	}
	
}
