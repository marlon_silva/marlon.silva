package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Servico_Contratado")
public class ServicoContratado {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "servicoContratado_seq", sequenceName = "servicoContratado_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column( name = "DESCRICAO", nullable = false )
	private String descricao;
	
	@Column( name = "VALOR", nullable = false )
	private long valor;
	
	@ManyToOne
	@JoinColumn(name = "corretor_id")
	private Corretor corretor;
	
	@OneToOne
	@JoinColumn(name = "segurados")
	private Segurados segurados;
	
	@OneToOne
	@JoinColumn(name = "servicos")
	private Servicos servicos;
	
	@OneToOne
	@JoinColumn(name = "seguradora")
	private Seguradora seguradora;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public long getValor() {
		return valor;
	}

	public void setValor(long valor) {
		this.valor = valor;
	}
		
	public Corretor getCorretor() {
		return corretor;
	}

	public void setCorretor(Corretor corretor) {
		this.corretor = corretor;
	}

	public Segurados getSegurados() {
		return segurados;
	}

	public void setSegurados(Segurados segurados) {
		this.segurados = segurados;
	}

	public Servicos getServicos() {
		return servicos;
	}

	public void setServicos(Servicos servicos) {
		this.servicos = servicos;
	}

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}
	
}
