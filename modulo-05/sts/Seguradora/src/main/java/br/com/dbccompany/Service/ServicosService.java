package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Servicos;
import br.com.dbccompany.Repository.ServicosRepository;

@Service
public class ServicosService {
	
	@Autowired
	private ServicosRepository servicosRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Servicos salvar(Servicos servicos) {
		return servicosRepository.save(servicos);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Optional<Servicos> bucarSegurados(long id){
		return servicosRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Servicos editarSegurados(long id, Servicos servicos) {
		return servicosRepository.save(servicos);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public List<Servicos> allServicos(){
		return (List<Servicos>) servicosRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Servicos buscarPorNome(String nome) {
		return servicosRepository.findByNome(nome);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Servicos buscarPorDescricao(String descricao) {
		return servicosRepository.findByNome(descricao);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Servicos buscarPorValorPadrao(long valorPadrao) {
		return servicosRepository.findByValorPadrao(valorPadrao);
	}
	
}
