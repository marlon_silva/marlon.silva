package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;


import br.com.dbccompany.Entity.Estado;

public interface EstadoRepository extends CrudRepository<Estado, Long> {
	
	Estado findByNome(String nome);

}
