package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cidade;

public interface CidadeRepository  extends CrudRepository<Cidade, Long>{

	Cidade findByNome(String nome);
}
