package br.com.dbccompany.Repository;






import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.ServicoContratado;

public interface CorretorRepository extends PessoasRepository<Corretor> {
	
	Corretor findByCargo(String cargo);
	Corretor findByComissao(long comissao);
	Corretor findByServicoContratados(ServicoContratado servicoContratado);
}
