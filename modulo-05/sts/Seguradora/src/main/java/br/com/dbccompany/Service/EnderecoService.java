package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Repository.EnderecoRepository;

@Service
public class EnderecoService {
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco salvar(Endereco endereco) {
		return enderecoRepository.save(endereco);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Optional<Endereco> buscarEndereco(long id){
		return enderecoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco editarEndereco(long id, Endereco endereco) {
		return enderecoRepository.save(endereco);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public List<Endereco> allEndereco(){
		return (List<Endereco>) enderecoRepository.findAll();
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco buscarPorLogradouro(String logradouro) {
		return enderecoRepository.findByLogradouro(logradouro);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco buscarPorNumero(long numero) {
		return enderecoRepository.findByNumero(numero);
	}
}
