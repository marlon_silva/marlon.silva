package br.com.dbccompany.Entity;

import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
public class Acessos {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "acessos_seq", sequenceName = "acessos_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)


    private long id;


    @Column(name = "IS_ENTRADA")
    private boolean isEntrada;

    @Column(name = "DATA")
    private LocalDate data;

    @Column(name = "IS_EXCECAO")
    private boolean isExcecao;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    private SaldoCliente saldoCliente;


    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setIsEntrada(boolean isEntradaentrada) {
        isEntrada = isEntrada;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public boolean getIsExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }
}
