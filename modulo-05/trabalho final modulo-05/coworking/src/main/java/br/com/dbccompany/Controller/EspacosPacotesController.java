package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.EspacoPocote;
import br.com.dbccompany.Enum.tipoContratacao;
import br.com.dbccompany.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/api/espacosPacotes")
public class EspacosPacotesController {

    @Autowired
    private EspacosPacotesService espacosPacotesService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<EspacoPocote> lstEspacosPacotes(){
        return espacosPacotesService.allEspacoPocote();
    }

    @PostMapping(value="/novo")
    @ResponseBody
    public EspacoPocote novoEspacoPocote( @RequestBody EspacoPocote espacoPocote) {
        return espacosPacotesService.salvar(espacoPocote);
    }

    @GetMapping(value="/{tipoContratacao}")
    @ResponseBody
    public EspacoPocote buscaEspecificaTipoContratacao(@PathVariable Enum<tipoContratacao> tipoContratacao) {
        return espacosPacotesService.buscarPorTipoContratacao(tipoContratacao);
    }

    @GetMapping(value="/{quantidade}")
    @ResponseBody
    public EspacoPocote buscaEspecificaQuantidade(@PathVariable long quantidade) {
        return espacosPacotesService.buscarPorQuantidade(quantidade);
    }

    @GetMapping(value="/{prazo}")
    @ResponseBody
    public EspacoPocote buscaEspecificaPrazo(@PathVariable Date prazo) {
        return espacosPacotesService.buscarPorPrazo(prazo);
    }
}
