package br.com.dbccompany.Entity;

import br.com.dbccompany.Enum.tipoContratacao;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ESPACOS_PACOTES")
public class EspacoPocote {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "espacoPacote_seq", sequenceName = "espacoPacote_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    private long id;

    @Column(name = "TIPO_CONTRATACAO", nullable = false)
    @Enumerated(EnumType.STRING)
    private tipoContratacao tipoContratacao;

    @Column(name = "QUANTIDADE", nullable =  false)
    private long quantidade;

    @Column(name = "PRAZO", nullable = false)
    private Date prazo;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public tipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(tipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public Date getPrazo() {
        return prazo;
    }

    public void setPrazo(Date prazo) {
        this.prazo = prazo;
    }
}
