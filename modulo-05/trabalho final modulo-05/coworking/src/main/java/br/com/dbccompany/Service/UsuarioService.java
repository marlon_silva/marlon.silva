package br.com.dbccompany.Service;


import br.com.dbccompany.Entity.Usuario;

import br.com.dbccompany.Repository.UsuarioRepository;
import br.com.dbccompany.Security.CriptografiaMD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository usuarioRepository;

    @Transactional(rollbackFor = Exception.class)
    public Usuario salvar(Usuario usuario) {
        if( usuario.getSenha().length() >= 6) {
            usuario.setSenha(CriptografiaMD5.criptografar(usuario.getSenha()));
            return usuarioRepository.save(usuario);
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<Usuario> buscarUsuario(long id){
        return usuarioRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Usuario editarUsuario(long id, Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Usuario> allUsuario(){
        return (List<Usuario>) usuarioRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Usuario buscarPorNome(String nome){return usuarioRepository.findByNome(nome);}

    @Transactional(rollbackFor = Exception.class)
    public Usuario buscarPorEmail(String email){return usuarioRepository.findByEmail(email);}

    @Transactional(rollbackFor = Exception.class)
    public Usuario buscarPorLogin(String login){return usuarioRepository.findByLogin(login);}

    @Transactional(rollbackFor = Exception.class)
    public Usuario buscarPorSenha(String senha){return usuarioRepository.findBySenha(senha);}
}
