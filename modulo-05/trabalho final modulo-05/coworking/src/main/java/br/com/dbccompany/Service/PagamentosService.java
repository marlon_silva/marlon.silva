package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Pacotes;
import br.com.dbccompany.Entity.Pagamentos;
import br.com.dbccompany.Enum.tipoPagamentos;
import br.com.dbccompany.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentosService {

    @Autowired
    private PagamentosRepository pagamentosRepository;

    @Transactional(rollbackFor = Exception.class)
    public Pagamentos salvar(Pagamentos pagamentos) {
        return pagamentosRepository.save(pagamentos);
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<Pagamentos> buscarPagamentos(long id){
        return pagamentosRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pagamentos editarPagamentos(long id, Pagamentos pagamentos) {
        return pagamentosRepository.save(pagamentos);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Pagamentos> allPagamentos(){
        return (List<Pagamentos>) pagamentosRepository.findAll();
    }



    @Transactional(rollbackFor = Exception.class)
    public Pagamentos buscarPorTipoPagamentos(Enum<tipoPagamentos> tipoPagamentos){return pagamentosRepository.findByTipoPagamentos(tipoPagamentos);}
}
