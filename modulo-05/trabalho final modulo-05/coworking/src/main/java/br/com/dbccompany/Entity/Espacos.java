package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Espacos {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "espacos_seq", sequenceName = "espacos_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    @Column(name = "espacos_id")
    private long id;
    @Column( name = "NOME", nullable = false )
    private String nome;

    @Column( name = "QTD_PESSOAS", nullable = false )
    private long qtdPessoas;

    @Column( name = "VALOR", nullable = false )
    private String valor;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(long qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }





}
