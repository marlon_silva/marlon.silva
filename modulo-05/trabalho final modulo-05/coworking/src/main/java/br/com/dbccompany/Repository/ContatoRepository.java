package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Long> {
    Contato findByValor (String valor);
    List<Contato> findByClientes(Clientes clientes);
}
