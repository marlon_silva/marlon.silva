package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Enum.tipoContratacao;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, Long> {

    SaldoCliente findByTipoContratacao(Enum<tipoContratacao> tipoContratacao);
    SaldoCliente findByQuantidade (long quantidade);
    SaldoCliente findByVenciamento(Date vencimento);
}
