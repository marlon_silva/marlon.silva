package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/api/acessos")
public class AcessosController {

    @Autowired
    private AcessosService acessosService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Acessos> lstAcessos(){
        return acessosService.allAcessos();
    }

    @PostMapping(value="/novo")
    @ResponseBody
    public Acessos novoAcesso( @RequestBody Acessos acessos) {
        return acessosService.salvar(acessos);
    }

    @GetMapping(value="/{isEntrada}")
    @ResponseBody
    public Acessos buscaEpecificaIsEntrada(@PathVariable boolean isEntrada) {
        return acessosService.buscarPorIsEntrada(isEntrada);
    }

    @GetMapping(value="/{data}")
    @ResponseBody
    public Acessos buscaEspecificaData(@PathVariable LocalDate data) {
        return acessosService.buscarPorData(data);
    }

    @GetMapping(value="/{isExcecao}")
    @ResponseBody
    public Acessos buscaEspecificaIsExcecao(@PathVariable boolean isExcecao) {
        return acessosService.buscarPorIsExcecao(isExcecao);
    }


}
