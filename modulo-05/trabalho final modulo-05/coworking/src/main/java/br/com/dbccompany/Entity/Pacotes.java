package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name =  "PACOTE")
public class Pacotes {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "paqote_seq", sequenceName = "paqote_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    private long id;

    @Column(name = "VALOR", nullable = false)
    private String  valor;

    @ManyToMany
    @JoinTable
            (name = "espacos_pacotes", joinColumns =
            @JoinColumn(name = "pacote_id"), inverseJoinColumns =
            @JoinColumn(name = "espacos_id"))
    private List<Espacos> espacos = new ArrayList<>();

    @ManyToMany
    @JoinTable(name="clientes_pacotes", joinColumns=
            @JoinColumn(name="pocotes_id"), inverseJoinColumns=
            @JoinColumn(name="clientes_id"))
    private List<Clientes> clientes = new ArrayList<>();

    public List<Clientes> getClientes() {
        return clientes;
    }

    public void pushClientes(Clientes...clientes) {
        this.clientes.addAll(Arrays.asList(clientes));
    }

    public List<Espacos> getEspacos() {
        return espacos;
    }

    public void pushEspacos(Espacos...espacos) {
        this.espacos.addAll(Arrays.asList(espacos));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

}
