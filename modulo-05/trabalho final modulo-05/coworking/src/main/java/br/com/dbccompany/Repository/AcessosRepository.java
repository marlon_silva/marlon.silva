package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Acessos;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.Date;

public interface AcessosRepository extends CrudRepository<Acessos, Long> {

    Acessos findByIsEntrada (boolean isEntrada);
    Acessos findByData (LocalDate data);
    Acessos findByIsExcecao (boolean isExcecao);

}
