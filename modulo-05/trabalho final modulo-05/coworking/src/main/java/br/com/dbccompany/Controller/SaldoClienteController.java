package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Enum.tipoContratacao;
import br.com.dbccompany.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/api/saldoCliente")
public class SaldoClienteController {

    @Autowired
    private SaldoClienteService saldoClienteService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<SaldoCliente> lstSaldoCliente(){
        return saldoClienteService.allSaldoCliente();
    }

    @PostMapping(value="/novo")
    @ResponseBody
    public SaldoCliente novoSaldoCliente( @RequestBody SaldoCliente saldoCliente) {
        return saldoClienteService.salvar(saldoCliente);
    }

    @GetMapping(value="/{tipoContratacao}")
    @ResponseBody
    public SaldoCliente buscaEspecificaTipoContratacao(@PathVariable Enum<tipoContratacao> tipoContratacao) {
        return saldoClienteService.buscarPorTipoContratacao(tipoContratacao);
    }

    @GetMapping(value="/{quantidade}")
    @ResponseBody
    public SaldoCliente buscaEspecificaQuantidade(@PathVariable long quantidade) {
        return saldoClienteService.buscarPorQuantidade(quantidade);
    }

    @GetMapping(value="/{vencimento}")
    @ResponseBody
    public SaldoCliente buscaEspecificaVencimento(@PathVariable Date vencimento) {
        return saldoClienteService.buscarPorVencimento(vencimento);
    }

}
