package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AcessosService {

    @Autowired
    private AcessosRepository acessosRepository;

    @Transactional(rollbackFor = Exception.class)
    public Acessos salvar(Acessos acessos) {
        return acessosRepository.save(acessos);
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<Acessos> buscarAcessos(long id){
        return acessosRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Acessos editarAcessos(long id,Acessos acessos) {
        return acessosRepository.save(acessos);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Acessos> allAcessos(){
        return (List<Acessos>) acessosRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Acessos buscarPorIsEntrada(Boolean isEntrada) {
        return acessosRepository.findByIsEntrada(isEntrada);
    }

    @Transactional(rollbackFor = Exception.class)
    public Acessos buscarPorData(LocalDate data){return acessosRepository.findByData(data);}

    @Transactional(rollbackFor = Exception.class)
    public Acessos buscarPorIsExcecao(Boolean isExcecao) {return acessosRepository.findByIsExcecao(isExcecao);}
}
