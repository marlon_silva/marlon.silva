package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Usuario> lstUsuario(){
        return usuarioService.allUsuario();
    }
    @PostMapping(value="/novo")
    @ResponseBody
    public Usuario novoUsuario( @RequestBody Usuario usuario) {
        return usuarioService.salvar(usuario);
    }

    @GetMapping(value="/buscarnome/{nome}")
    @ResponseBody
    public Usuario buscaEspecificaNome(@PathVariable String nome) {
        return usuarioService.buscarPorNome(nome);
    }


    @GetMapping(value="/buscaremail/{email}")
    @ResponseBody
    public Usuario buscaEspecificaEmail(@PathVariable String email) {
        return usuarioService.buscarPorEmail(email);
    }

    @GetMapping(value="/buscarlogin/{login}")
    @ResponseBody
    public Usuario buscaEspecificaLogin(@PathVariable String login) {
        return usuarioService.buscarPorLogin(login);
    }

}
