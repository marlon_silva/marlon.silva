package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;

public interface PacotesRepository extends CrudRepository<Pacotes, Long> {

    Pacotes findByValor (String valor);

}
