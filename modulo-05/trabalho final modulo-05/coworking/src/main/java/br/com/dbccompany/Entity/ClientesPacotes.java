package br.com.dbccompany.Entity;

import javax.persistence.*;

@Entity
@Table(name = "CLIENTE_PACOTES")
public class ClientesPacotes {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "clientesPacotes_seq", sequenceName = "clientesPacotes_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    private long id;

    @Column(name = "QUANTIDADE")
    private long quantidade;

    @OneToOne
    @JoinColumn(name = "pagamentos_id")
    private Pagamentos pagamentos;

    public Pagamentos getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(Pagamentos pagamentos) {
        this.pagamentos = pagamentos;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }
}
