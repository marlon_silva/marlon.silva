package br.com.dbccompany.Entity;

import javax.persistence.*;


import java.io.Serializable;

@Embeddable
public class SaldoClienteId implements Serializable {

    @ManyToOne(cascade = CascadeType.ALL)
    private Clientes clientes;

    @ManyToOne(cascade = CascadeType.ALL)
    private Espacos espacos;



    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

}
