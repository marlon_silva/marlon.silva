package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.ClientesPacotes;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Repository.ClientesPacotesRepository;
import br.com.dbccompany.Repository.ClientesRepository;
import br.com.dbccompany.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    @Transactional(rollbackFor = Exception.class)
    public Contato salvar(Contato contato) {
        return contatoRepository.save(contato);
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<Contato> buscarContato(long id){
        return contatoRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Contato editarContato(long id, Contato contato) {
        return contatoRepository.save(contato);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Contato> allContato(){
        return (List<Contato>) contatoRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Contato buscarPorValor(String valor){return contatoRepository.findByValor(valor);}

}
