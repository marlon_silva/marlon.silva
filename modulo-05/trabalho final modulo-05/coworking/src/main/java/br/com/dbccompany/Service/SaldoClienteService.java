package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Enum.tipoContratacao;
import br.com.dbccompany.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente salvar(SaldoCliente saldoCliente) {
        return saldoClienteRepository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<SaldoCliente> buscarSaldoCliente(long id){
        return saldoClienteRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente editarSaldoCliente(long id, SaldoCliente saldoCliente) {
        return saldoClienteRepository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<SaldoCliente> allSaldoCliente(){
        return (List<SaldoCliente>) saldoClienteRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente buscarPorTipoContratacao(Enum<tipoContratacao> tipoContratacao){return saldoClienteRepository.findByTipoContratacao(tipoContratacao);}

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente buscarPorQuantidade(long quantidade){return saldoClienteRepository.findByQuantidade(quantidade);}



    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente buscarPorVencimento(Date vencimento){return saldoClienteRepository.findByVenciamento(vencimento);}
}
