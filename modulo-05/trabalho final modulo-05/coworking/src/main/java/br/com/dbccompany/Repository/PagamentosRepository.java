package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Pagamentos;
import br.com.dbccompany.Enum.tipoPagamentos;
import org.springframework.data.repository.CrudRepository;

public interface PagamentosRepository extends CrudRepository<Pagamentos, Long> {

    Pagamentos findByTipoPagamentos (Enum<tipoPagamentos> tipoPagamentos);

}
