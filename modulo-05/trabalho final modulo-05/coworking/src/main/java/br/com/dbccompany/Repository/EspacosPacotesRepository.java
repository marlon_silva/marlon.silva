package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.EspacoPocote;
import br.com.dbccompany.Enum.tipoContratacao;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface EspacosPacotesRepository extends CrudRepository<EspacoPocote, Long> {

    EspacoPocote findByTipoContratacao (Enum<tipoContratacao> tipoContratacao);
    EspacoPocote findByQuantidade (long quantidade);
    EspacoPocote findByPrazo (Date prazo);

}
