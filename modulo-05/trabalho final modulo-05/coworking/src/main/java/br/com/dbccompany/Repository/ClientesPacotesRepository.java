package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.ClientesPacotes;
import org.springframework.data.repository.CrudRepository;

public interface ClientesPacotesRepository extends CrudRepository<ClientesPacotes, Long> {

    ClientesPacotes findByQuantidade (long quantidade);
}
