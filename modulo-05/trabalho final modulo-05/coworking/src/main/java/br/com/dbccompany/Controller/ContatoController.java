package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contato> lstContato(){
        return contatoService.allContato();
    }
    @PostMapping(value="/novo")
    @ResponseBody
    public Contato novoContato( @RequestBody Contato contato) {
        return contatoService.salvar(contato);
    }

    @GetMapping(value="/{valor}")
    @ResponseBody
    public Contato buscaEspecificaValor(@PathVariable String valor) {
        return contatoService.buscarPorValor(valor);
    }

}
