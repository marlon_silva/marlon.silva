package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Enum.tipoContratacao;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface ContratacaoRepository extends CrudRepository<Contratacao, Long> {

    Contratacao findByTipoContratacao (Enum<tipoContratacao> tipoContratacao);
    Contratacao findByQuantidade (long quantidade);
    Contratacao findByDesconto (long desconto);
    Contratacao findByPrazo (Date prazo);

}
