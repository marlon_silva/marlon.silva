package br.com.dbccompany.Entity;

import br.com.dbccompany.Enum.tipoContratacao;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
@Table(name ="SALDO_CLIENTE")
public class SaldoCliente implements Serializable {

    @EmbeddedId
    private SaldoClienteId saldoClienteId;

    @Column(name = "TIPO_CONTRATACAO", nullable = false)
    @Enumerated(EnumType.STRING)
    private tipoContratacao tipoContratacao;

    @Column(name = "QUANTIDADE", nullable = false)
    private long quantidade;

    @Column(name = "VENCIMENTO", nullable = false)
    private Date venciamento;

    @OneToMany(mappedBy = "saldoCliente", cascade = CascadeType.MERGE)
    private List<Acessos> acessos;

    public List<Acessos> getAcessos() {
        return acessos;
    }

    public void pushAcessos(Acessos...acessos) {
        this.acessos.addAll(Arrays.asList(acessos));
    }

    public SaldoClienteId getSaldoClienteId() {
        return saldoClienteId;
    }

    public void setSaldoClienteId(SaldoClienteId saldoClienteId) {
        this.saldoClienteId = saldoClienteId;
    }

    public tipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(tipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVenciamento() {
        return venciamento;
    }

    public void setVenciamento(Date venciamento) {
        this.venciamento = venciamento;
    }
}
