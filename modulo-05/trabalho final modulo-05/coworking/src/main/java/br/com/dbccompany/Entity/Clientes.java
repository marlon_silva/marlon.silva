package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Clientes  {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "clientes_seq", sequenceName = "clientes_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    @Column(name = "cliente_id")
    private long id;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @Column( name = "CPF", nullable = false )
    private long cpf;

    @Column( name = "DATA_NASCIMENTO", nullable = false )
    private LocalDate dataNascimento;

    @OneToMany
    @JoinColumn(name = "contato_id")
    private List<Contato> contato = new ArrayList<>();

    public List<Contato> getContato() {
        return contato;
    }

    public void pushContato(Contato...contato) {
        this.contato.addAll(Arrays.asList(contato));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCpf() {
        return cpf;
    }

    public void setCpf(long cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
