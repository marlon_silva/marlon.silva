package br.com.dbccompany.Entity;

import javax.persistence.*;

@Entity
public class Contato {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "contato_seq", sequenceName = "contato_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    @Column(name = "contato_id", nullable = false)
    private long id;
    @Column(name = "VALOR", nullable = false)
    private String valor;

    @OneToOne
    @JoinColumn(name = "tipo_contato_id")
    private TipoContato tipoContato;

    @ManyToOne
    @JoinColumn(name = "clientes_id")
    private Clientes clientes;

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }
}
