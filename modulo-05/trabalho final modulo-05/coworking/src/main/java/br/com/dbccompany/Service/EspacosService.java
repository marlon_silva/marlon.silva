package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.EspacoPocote;
import br.com.dbccompany.Entity.Espacos;
import br.com.dbccompany.Enum.tipoContratacao;
import br.com.dbccompany.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosService {

    @Autowired
    private EspacosRepository espacosRepository;

    @Transactional(rollbackFor = Exception.class)
    public Espacos salvar(Espacos espacos) {
        return espacosRepository.save(espacos);
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<Espacos> buscarEspacos(long id){
        return espacosRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Espacos editarEspacos(long id, Espacos espacos) {
        return espacosRepository.save(espacos);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Espacos> allEspacos(){
        return (List<Espacos>) espacosRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Espacos buscarPorNome(String nome){return espacosRepository.findByNome(nome);}

    @Transactional(rollbackFor = Exception.class)
    public Espacos buscarPorQtdPessoas(long qtdPessoas){return espacosRepository.findByQtdPessoas(qtdPessoas);}

    @Transactional(rollbackFor = Exception.class)
    public Espacos buscarPorValor(String valor){return espacosRepository.findByValor(valor);}



}
