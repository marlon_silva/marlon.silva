package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Pacotes;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")
public class TipoContatoController {

    @Autowired
    private TipoContatoService tipoContatoService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<TipoContato> lstTipoContato(){
        return tipoContatoService.allTipoContato();
    }

    @PostMapping(value="/novo")
    @ResponseBody
    public TipoContato novoTipoContato( @RequestBody TipoContato tipoContato) {
        return tipoContatoService.salvar(tipoContato);
    }


    @GetMapping(value="/{nome}")
    @ResponseBody
    public TipoContato buscaEspecificaNome(@PathVariable String nome) {
        return tipoContatoService.buscarPorNome(nome);
    }
}
