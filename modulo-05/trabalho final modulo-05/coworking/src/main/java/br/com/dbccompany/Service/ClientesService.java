package br.com.dbccompany.Service;


import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Repository.ClientesRepository;
import br.com.dbccompany.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {
    @Autowired
    private ClientesRepository clientesRepository;
    @Autowired
    private ContatoRepository contatoRepository;

    @Transactional(rollbackFor = Exception.class)

    public Clientes salvar(Clientes clientes) {

        boolean email = false;
        boolean telefone = false;
        for (Contato contato :  contatoRepository.findByClientes(clientes)) {
            if(contato.getTipoContato().getNome().equals("EMAIL")){ email = true; }
            if(contato.getTipoContato().getNome().equals("TELEFONE")){ telefone = true; }
        }
        if(email && telefone) { return clientesRepository.save(clientes); }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<Clientes> buscarClientes(long id){
        return clientesRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Clientes editarClientes(long id, Clientes clientes) {
        return clientesRepository.save(clientes);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Clientes> allClientes(){
        return (List<Clientes>) clientesRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Clientes buscarPorNome(String nome){return clientesRepository.findByNome(nome);}

    @Transactional(rollbackFor = Exception.class)
    public Clientes buscarPorCpf(long cpf) {return clientesRepository.findByCpf(cpf);}

    @Transactional(rollbackFor = Exception.class)
    public Clientes buscarPorDataNascimento(LocalDate dataNascimento) {return clientesRepository.findByDataNascimento(dataNascimento);}
}
