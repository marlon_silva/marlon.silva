package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Espacos;
import br.com.dbccompany.Entity.Pacotes;
import br.com.dbccompany.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PacotesService {


    @Autowired
    private PacotesRepository pacotesRepository;

    @Transactional(rollbackFor = Exception.class)
    public Pacotes salvar(Pacotes pacotes) {
        return pacotesRepository.save(pacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<Pacotes> buscarPacotes(long id){
        return pacotesRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pacotes editarEspacos(long id, Pacotes pacotes) {
        return pacotesRepository.save(pacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<Pacotes> allEspacos(){
        return (List<Pacotes>) pacotesRepository.findAll();
    }



    @Transactional(rollbackFor = Exception.class)
    public Pacotes buscarPorValor(String valor){return pacotesRepository.findByValor(valor);}
}
