package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.ClientesPacotes;
import br.com.dbccompany.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientesPacotes")
public class ClientesPacotesController {

    @Autowired
    private ClientesPacotesService clientesPacotesService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<ClientesPacotes> lstClientesPacotes(){
        return clientesPacotesService.allClientesPacotes();
    }
    @PostMapping(value="/novo")
    @ResponseBody
    public ClientesPacotes novoClientesPacots( @RequestBody ClientesPacotes clientesPacotes) {
        return clientesPacotesService.salvar(clientesPacotes);
    }

    @GetMapping(value="/{quantidade}")
    @ResponseBody
    public ClientesPacotes buscaEspecifica(@PathVariable long quantidade) {
        return clientesPacotesService.buscarPorQuantidade(quantidade);
    }

}
