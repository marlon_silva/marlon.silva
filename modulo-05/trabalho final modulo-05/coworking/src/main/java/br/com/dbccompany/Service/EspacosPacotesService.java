package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.EspacoPocote;
import br.com.dbccompany.Enum.tipoContratacao;
import br.com.dbccompany.Repository.EspacosPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class EspacosPacotesService {
    @Autowired
    private EspacosPacotesRepository espacosPacotesRepository;

    @Transactional(rollbackFor = Exception.class)
    public EspacoPocote salvar(EspacoPocote espacoPocote) {
        return espacosPacotesRepository.save(espacoPocote);
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<EspacoPocote> buscarEspacoPocote(long id){
        return espacosPacotesRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public EspacoPocote editarEspacoPocote(long id, EspacoPocote espacoPocote) {
        return espacosPacotesRepository.save(espacoPocote);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<EspacoPocote> allEspacoPocote(){
        return (List<EspacoPocote>) espacosPacotesRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public EspacoPocote buscarPorTipoContratacao(Enum<tipoContratacao> tipoContratacao){return espacosPacotesRepository.findByTipoContratacao(tipoContratacao);}

    @Transactional(rollbackFor = Exception.class)
    public EspacoPocote buscarPorQuantidade(long quantidade){return espacosPacotesRepository.findByQuantidade(quantidade);}


    @Transactional(rollbackFor = Exception.class)
    public EspacoPocote buscarPorPrazo(Date prazo){return espacosPacotesRepository.findByPrazo(prazo);}
}
