package br.com.dbccompany.Entity;

import javax.persistence.*;

@Entity
@Table(name = "TIPO_CONTATO")
public class TipoContato {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "tipo_contato_seq", sequenceName = "tipo_contato_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    private long id;
    private String nome;

    @OneToOne
    @JoinColumn(name = "Contato")
    private Contato contato;

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
