package br.com.dbccompany.Entity;

import br.com.dbccompany.Enum.tipoContratacao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Contratacao {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "contratacao_seq", sequenceName = "contratacao_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    private long id;

    @Column(name = "TIPO_CONTRATACAO", nullable = false)
    @Enumerated(EnumType.STRING)
    private tipoContratacao tipoContratacao;

    @Column(name = "QUANTIDADE", nullable = false)
    private long quantidade;

    @Column(name = "DESCONTO", nullable = false)
    private long desconto;

    @Column(name = "PRAZO", nullable = false)
    private Date prazo;



    @OneToOne
    @JoinColumn(name = "pagamento_id")
    private Pagamentos pagamentos;

    public Date getPrazo() {
        return prazo;
    }

    public void setPrazo(Date prazo) {
        this.prazo = prazo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public br.com.dbccompany.Enum.tipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(br.com.dbccompany.Enum.tipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public long getDesconto() {
        return desconto;
    }

    public void setDesconto(long desconto) {
        this.desconto = desconto;
    }



}
