package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.ClientesPacotes;
import br.com.dbccompany.Repository.ClientesPacotesRepository;
import br.com.dbccompany.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
@Service
public class ClientesPacotesService {
    @Autowired
    private ClientesPacotesRepository clientesPacotesRepository;

    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotes salvar(ClientesPacotes clientesPacotes) {
        return clientesPacotesRepository.save(clientesPacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<ClientesPacotes> buscarClientesPacotes(long id){
        return clientesPacotesRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotes editarClientesPacotes(long id, ClientesPacotes clientesPacotes) {
        return clientesPacotesRepository.save(clientesPacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<ClientesPacotes> allClientesPacotes(){
        return (List<ClientesPacotes>) clientesPacotesRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotes buscarPorQuantidade(long quantidade){return clientesPacotesRepository.findByQuantidade(quantidade);}
}
