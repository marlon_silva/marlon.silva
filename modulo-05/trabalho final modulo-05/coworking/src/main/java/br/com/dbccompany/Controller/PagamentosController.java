package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Pacotes;
import br.com.dbccompany.Entity.Pagamentos;
import br.com.dbccompany.Enum.tipoPagamentos;
import br.com.dbccompany.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamentos")
public class PagamentosController {

    @Autowired
    private PagamentosService pagamentosService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pagamentos> lstPagamentos(){
        return pagamentosService.allPagamentos();
    }

    @PostMapping(value="/novo")
    @ResponseBody
    public Pagamentos novoPagamentos( @RequestBody Pagamentos pagamentos) {
        return pagamentosService.salvar(pagamentos);
    }


    @GetMapping(value="/{tipoPagamentos}")
    @ResponseBody
    public Pagamentos buscaEspecificaTipoPagamentos(@PathVariable Enum<tipoPagamentos> tipoPagamentos) {
        return pagamentosService.buscarPorTipoPagamentos(tipoPagamentos);
    }
}
