package br.com.dbccompany.Entity;

import br.com.dbccompany.Enum.tipoPagamentos;

import javax.persistence.*;

@Entity
public class Pagamentos {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "pagamentos_seq", sequenceName = "pagamentos_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    private long id;

    @Column(name = "TIPO_PAGAMENTO", nullable = false)
    @Enumerated(EnumType.STRING)
    private br.com.dbccompany.Enum.tipoPagamentos tipoPagamentos;

    @OneToOne
    @JoinColumn(name = "contratacao_id")
    private Contratacao contratacao;

    @OneToOne
    @JoinColumn(name = "cliente_pacote_id")
    private ClientesPacotes clientesPacotes;

    public ClientesPacotes getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotes clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public tipoPagamentos getTipoPagamentos() {
        return tipoPagamentos;
    }

    public void setTipoPagamentos(tipoPagamentos tipoPagamentos) {
        this.tipoPagamentos = tipoPagamentos;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


}
