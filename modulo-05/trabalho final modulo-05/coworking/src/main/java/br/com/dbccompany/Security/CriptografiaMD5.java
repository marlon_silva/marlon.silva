package br.com.dbccompany.Security;

import java.math.BigInteger;
import java.security.MessageDigest;

public class CriptografiaMD5 {
    public static String criptografar(String senha) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(senha.getBytes(), 0, senha.length());
            BigInteger senhaMd5 = new BigInteger(1, m.digest());
            return senha = String.format("%1$032X", senhaMd5);
        } catch( Exception e ) {
            //Senha n pode ser nula n pode salvar o cara, moro!!
            return null;
        }
    }
}