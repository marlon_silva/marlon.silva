package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Clientes;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.Date;

public interface ClientesRepository extends CrudRepository<Clientes, Long> {

    Clientes findByNome (String nome);
    Clientes findByCpf (long cpf);
    Clientes findByDataNascimento (LocalDate dataNascimento);
}
