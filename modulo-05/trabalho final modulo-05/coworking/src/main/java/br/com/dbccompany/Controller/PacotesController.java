package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Espacos;
import br.com.dbccompany.Entity.Pacotes;
import br.com.dbccompany.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacotes")
public class PacotesController {

    @Autowired
    private PacotesService pacotesService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pacotes> lstEspacos(){
        return pacotesService.allEspacos();
    }

    @PostMapping(value="/novo")
    @ResponseBody
    public Pacotes novoPacote( @RequestBody Pacotes pacotes) {
        return pacotesService.salvar(pacotes);
    }


    @GetMapping(value="/{valor}")
    @ResponseBody
    public Pacotes buscaEspecificaValor(@PathVariable String valor) {
        return pacotesService.buscarPorValor(valor);
    }
}
