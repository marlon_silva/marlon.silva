package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.ClientesRepository;
import br.com.dbccompany.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Transactional(rollbackFor = Exception.class)
    public TipoContato salvar(TipoContato tipoContato) {
        tipoContato.setNome(tipoContato.getNome().toUpperCase());
        return tipoContatoRepository.save(tipoContato);
    }

    @Transactional(rollbackFor = Exception.class)
    public Optional<TipoContato> buscarTipoContato(long id){
        return tipoContatoRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public TipoContato editarTipoContato(long id, TipoContato tipoContato) {
        return tipoContatoRepository.save(tipoContato);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<TipoContato> allTipoContato(){
        return (List<TipoContato>) tipoContatoRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public TipoContato buscarPorNome(String nome){return tipoContatoRepository.findByNome(nome);}
}
