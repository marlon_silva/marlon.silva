package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/api/clientes")
public class ClientesController {

    @Autowired
    private ClientesService clientesService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Clientes> lstCLientes(){
        return clientesService.allClientes();
    }
    @PostMapping(value="/novo")
    @ResponseBody
    public Clientes novoCliente( @RequestBody Clientes clientes) {
        return clientesService.salvar(clientes);
    }

    @GetMapping(value="/{nome}")
    @ResponseBody
    public Clientes buscaEspecificaNome(@PathVariable String nome) {
        return clientesService.buscarPorNome(nome);
    }

    @GetMapping(value="/{cpf}")
    @ResponseBody
    public Clientes buscaEspecificaCpf(@PathVariable long cpf) {
        return clientesService.buscarPorCpf(cpf);
    }

    @GetMapping(value="/{dataNascimento}")
    @ResponseBody
    public Clientes buscaEspecificaDataNascimentof(@PathVariable LocalDate dataNascimento) {
        return clientesService.buscarPorDataNascimento(dataNascimento);
    }


}
