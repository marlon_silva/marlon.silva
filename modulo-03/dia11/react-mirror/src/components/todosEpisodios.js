<<<<<<< HEAD
import React from 'react'
import { Link } from 'react-router-dom'

const TodosEpisodios = props => {
  const { listaEpisodios } = props.location.state
  return listaEpisodios.todos.map( e => <Link className="link campo" to={{ pathname:'/telaDetalhesEpisodio', state: { e } }}
value={ e.id }><li key={ e.id }>{ `${ e.nome } -Duração: ${ e.duracao } -Temporada: ${ e.temporada } ` }</li></Link> )


}
export default TodosEpisodios
=======
import React, { Component } from 'react'

import ListaEpisodios from './shared/listaEpisodios'
import MeuBotao from './shared/meuBotao'
import CampoDeBusca from './shared/campoDeBusca'

import EpisodiosApi from '../models/episodiosApi'

const ASC = 'ASC', DESC = 'DESC'

export default class TodosEpisodios extends Component {

  constructor( props ) {
    super( props )
    this.episodiosApi = new EpisodiosApi()
    this.state = {
      // geramos uma função de ordenação inicial vazia (para não alterar a ordem natural da listagem que recebemos por props)
      // aí quando clicarmos nos botões, trocaremos esta função e repassaremos para o componente de listagem
      ordenacao: () => { },
      tipoOrdenacaoDataEstreia: ASC,
      tipoOrdenacaoDuracao: ASC,
      // aqui o concat é utilizado para gerar uma cópia em memória do array, para não alterarmos o original
      listaEpisodios: this.props.location.state.listaEpisodios.todos.concat([])
    }
  }

  componentDidMount() {
    const { listaEpisodios } = this.props.location.state
    // esse map está transformando cada episódio em um Promise para pegar seus detalhes
    // const requisicoes será um array de promises, para que as requisições possam ser feitas em paralelo
    const requisicoes = listaEpisodios.todos.map( e => this.episodiosApi.buscarDetalhes( e.id ) )
    // Aqui nós "juntamos" todas as respostas. Ou seja, Promise.all espera todas requisições retornarem, e nos dá um array de resultados em ordem de inserção no array requisições
    Promise.all( requisicoes ).then( resultados => {
      listaEpisodios.todos.forEach( episodio => {
        episodio.dataEstreia = resultados.find( e => e.episodioId === episodio.id ).dataEstreia
      } )
    } )
  }
 
  alterarOrdenacaoParaDataEstreia = () => {
    const { tipoOrdenacaoDataEstreia } = this.state
    this.setState( {
      ordenacao: ( a, b ) => new Date( ( tipoOrdenacaoDataEstreia === ASC ? a : b ).dataEstreia ) - new Date( ( tipoOrdenacaoDataEstreia === ASC ? b : a ).dataEstreia ),
      tipoOrdenacaoDataEstreia: tipoOrdenacaoDataEstreia === ASC ? DESC : ASC
    } )
  }

  alterarOrdenacaoParaDuracao = () => {
    const { tipoOrdenacaoDuracao } = this.state
    this.setState( {
      ordenacao: ( a, b ) => ( tipoOrdenacaoDuracao === ASC ? a : b ).duracao - ( tipoOrdenacaoDuracao === ASC ? b : a ).duracao,
      tipoOrdenacaoDuracao: tipoOrdenacaoDuracao === ASC ? DESC : ASC
    } )
  }

  filtrarPorTermo = evt => {
    // sempre consultamos a lista original para evitar sub filtros (ex: filtrar por ministro e depois mundo, ele procurará cumulativamente)
    const todosEpisodios = this.props.location.state.listaEpisodios.todos
    const termo = evt.target.value
    this.episodiosApi.filtrarPorTermo( termo )
      .then( resultados => {
        this.setState( {
          listaEpisodios: todosEpisodios.filter( e => resultados.some( r => r.episodioId === e.id ) )
        } )
      } )
  }

  render() {
    const { listaEpisodios } = this.state
    listaEpisodios.sort( this.state.ordenacao )
    return <React.Fragment>
      <h1>Todos episódios</h1>
      <CampoDeBusca atualizarValor={ this.filtrarPorTermo } placeholder="ex: ministro"/>
      <div className="botoes">
        <MeuBotao cor="azul" quandoClicar={ this.alterarOrdenacaoParaDataEstreia } texto="Ordenar por data de estreia"/> 
        <MeuBotao cor="verde" quandoClicar={ this.alterarOrdenacaoParaDuracao } texto="Ordenar por duração"/> 
      </div>
      <ListaEpisodios listaEpisodios={ listaEpisodios } />
    </React.Fragment>
  }
}
>>>>>>> aeac8c72d577b3e1039bead17dee4babcfb5ba83
