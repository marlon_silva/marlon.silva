import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

const ListaEpisodios = ( { listaEpisodios } ) =>
  <React.Fragment>
    {
      listaEpisodios && listaEpisodios.map( e =>
        <li key={ e.id }>
          <Link to={ { pathname: `/episodio/${ e.id }`, state: { episodio: e } } }>
            { `${ e.nome } - ${ e.nota || 'Sem nota' }` }
          </Link>
        </li>
      )
    }
  </React.Fragment>

ListaEpisodios.propTypes = {
  listaEpisodios: PropTypes.array.isRequired
}

export default ListaEpisodios
