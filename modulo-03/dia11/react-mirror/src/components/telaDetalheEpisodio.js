<<<<<<< HEAD
import React from 'react'
import { Link } from 'react-router-dom'


const TelaDetalhesEpisodios= props => {
  const { listaEpisodios } = props.location.state
  return listaEpisodios.avaliados.map( e => <Link className="link" to={{ pathname: "/episodio", state: { e } }} value={ e.id }><li key={ e.id }>{ `${ e.nome } - ${ e.nota } - ${ e.temporada } - ${ e.ordemEpisodio }` }</li></Link> )
}

export default TelaDetalhesEpisodios
=======
import React, { Component } from 'react'
import EpisodioUi from './episodioUi'
import EpisodiosApi from '../models/episodiosApi'

export default class TelaDetalheEpisodio extends Component {

  constructor( props ) {
    super( props )
    this.episodiosApi = new EpisodiosApi()
    this.state = {
      detalhes: null
    }
  }

  componentDidMount() {
    const episodioId = this.props.match.params.id
    const requisicoes = [
      this.episodiosApi.buscarDetalhes( episodioId ),
      this.episodiosApi.buscarNota( episodioId )
    ]
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all
    Promise.all( requisicoes )
      .then( respostas => {
        this.setState( {
          // acessamos a posição 0 dentro de respostas, pois é isso que o Promise.all faz
          // ele retorna um array com o resultado de todas requisições, em ordem de inserção no array requisicoes
          detalhes: respostas[ 0 ], 
          objNota: respostas[ 1 ]
        } )
      } )
  }

  render() {
    const { episodio } = this.props.location.state
    const { detalhes, objNota } = this.state
    // TODO: passar regras de formatação de detalhes para episódio
    //const notaImdb = detalhes.notaImdb * 0.5
    return <React.Fragment>
      <EpisodioUi episodio={ episodio } />
      {
        detalhes ?
          <React.Fragment>
            <p>{ detalhes.sinopse }</p>
            <span>{ new Date( detalhes.dataEstreia ).toLocaleDateString() }</span>
            <span>IMDb: { detalhes.notaImdb * 0.5 }</span>
            <span>Sua nota: { objNota ? objNota.nota : 'N/D' }</span>
          </React.Fragment> : null
      }
    </React.Fragment>
  }
}
>>>>>>> aeac8c72d577b3e1039bead17dee4babcfb5ba83
