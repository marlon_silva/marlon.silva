console.log( 'Bem vindx' )

function renderizaPokemonNaTela( pokemon ) {
  const dadosPokemon = document.getElementById( 'dadosPokemon' )
  const nome = dadosPokemon.querySelector( '.nome' )
  nome.innerText = pokemon.nome
  const imgPokemon = dadosPokemon.querySelector( '.thumb' )
  imgPokemon.src = pokemon.thumbUrl
  console.log( pokemon.altura )
}

const pokeApi = new PokeApi()
// pokeApi.buscar( 123 )
//   .then( pokemonServidor => {
//     const poke = new Pokemon( pokemonServidor )
//     renderizaPokemonNaTela( poke )
//   } )

async function rodar() {
  const pokemonServidor = await pokeApi.buscar( 123 )
  const poke = new Pokemon( pokemonServidor )
  renderizaPokemonNaTela( poke )
}
rodar()

console.log( 'isso vai rodar antes ou depois?' )
