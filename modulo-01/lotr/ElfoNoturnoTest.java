import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest {

    private final double DELTA = 1e-9;

    @Test
    public void elfoNoturnoSemAtirarNaoGanhaXp() {
        Elfo elfoNoturno = new ElfoNoturno("Noturno!");
        assertEquals(0, elfoNoturno.getExperiencia());
    }

    @Test
    public void elfoNoturnoGanha3AoAtirarFlecha() {
        Elfo elfoNoturno = new ElfoNoturno("Noturno!");
        elfoNoturno.atirarFlecha(new Dwarf("Balin"));
        assertEquals(3, elfoNoturno.getExperiencia());
    }

    @Test
    public void elfoNoturnoAtiraFlechaEPerde15() {
        ElfoNoturno noturno = new ElfoNoturno("Night Elf");
        noturno.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(85.0, noturno.getVida(), DELTA);
        assertEquals(Status.SOFREU_DANO, noturno.getStatus());
    }

    @Test
    public void elfoNoturnoAtira7FlechasEMorre() {
        ElfoNoturno noturno = new ElfoNoturno("Night Elf");
        // gameshark:
        noturno.getFlecha().setQuantidade(1000);
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(.0, noturno.getVida(), DELTA);
        assertEquals(Status.MORTO, noturno.getStatus());
    }

    @Test
    public void elfoNoturnoAtira8FlechasEMorre() {
        ElfoNoturno noturno = new ElfoNoturno("Night Elf");
        // gameshark:
        noturno.getFlecha().setQuantidade(1000);
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        noturno.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(.0, noturno.getVida(), DELTA);
        assertEquals(Status.MORTO, noturno.getStatus());
    }
}


